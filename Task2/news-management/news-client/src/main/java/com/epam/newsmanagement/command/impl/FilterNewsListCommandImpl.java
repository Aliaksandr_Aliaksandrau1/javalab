package com.epam.newsmanagement.command.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.command.CommandException;
import com.epam.newsmanagement.command.ICommand;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManageService;
import com.epam.newsmanagement.service.impl.NewsManageServiceImpl;
import com.epam.newsmanagement.utility.SearchCriteria;

public class FilterNewsListCommandImpl implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		
		
		
		String page = null;

		ConfigurableApplicationContext ctx = (ClassPathXmlApplicationContext) request.getServletContext()
				.getAttribute("ctx");
		NewsManageService newsManageService = (NewsManageServiceImpl) ctx.getBean("newsManageService");

		
		SearchCriteria sc = new SearchCriteria();
		
		List<NewsTO> newsList = null;
		List<Tag> tagList = null;
		List<Author> authorList = null;

		String[] tagIdArray = request.getParameterValues("tagId");
		String authorId = request.getParameter("authorId");

		List<Long> tagIdList = new ArrayList<Long>();
		
		if (tagIdArray != null) {
			for (int i = 0; i < tagIdArray.length; i++) {
				System.out.println("tag id " + tagIdArray[i]);
				tagIdList.add(new Long(tagIdArray[i]));
			}
			
			sc.setTagIdList(tagIdList);
			
		}

		if (authorId != null && authorId!="") {

			System.out.println("author id : " + authorId);
			sc.setAuthorId(new Long(authorId));
		}

		try {
			newsList = newsManageService.readNewsBySearchCriteria(sc);
			tagList = newsManageService.readTags();
			authorList = newsManageService.readAllAuthors();

		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		request.setAttribute("newsList", newsList);
		request.setAttribute("tagList", tagList);
		request.setAttribute("authorList", authorList);

		System.out.println("Filter_NEWS_LIST");
		page = "/WEB-INF/views/view-news-list.jsp";

		return page;
	}

}
