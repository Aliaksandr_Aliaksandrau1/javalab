package com.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.command.CommandException;
import com.epam.newsmanagement.command.ICommand;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManageService;
import com.epam.newsmanagement.service.impl.NewsManageServiceImpl;

public class ViewNewsCommandImpl implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;

		ConfigurableApplicationContext ctx = (ClassPathXmlApplicationContext) request.getServletContext()
				.getAttribute("ctx");
		NewsManageService newsManageService = (NewsManageServiceImpl) ctx.getBean("newsManageService");
		NewsTO news = null;
		try {
			Long newsId = new Long(request.getParameter("newsId"));

			System.out.println(newsId);

			news = newsManageService.readNewsById(newsId);

		} catch (ServiceException e) {
			throw new CommandException (e);
		}
		request.setAttribute("news", news);

		page = "/WEB-INF/views/view-news.jsp";
		return page;
	}

}
