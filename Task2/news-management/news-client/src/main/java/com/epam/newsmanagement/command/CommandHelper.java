package com.epam.newsmanagement.command;

import java.util.HashMap;
import java.util.Map;

import com.epam.newsmanagement.command.impl.AddCommentCommandImpl;
import com.epam.newsmanagement.command.impl.ChangeLocaleCommand;
import com.epam.newsmanagement.command.impl.DefaultCommandImpl;
import com.epam.newsmanagement.command.impl.FilterNewsListCommandImpl;
import com.epam.newsmanagement.command.impl.ViewNewsCommandImpl;
import com.epam.newsmanagement.command.impl.ViewNewsListCommandImpl;

public class CommandHelper {
	private Map<CommandEnum, ICommand> commandMap = new HashMap<>();

	private CommandHelper() {
		commandMap.put(CommandEnum.VIEW_NEWS_LIST, new ViewNewsListCommandImpl());
		commandMap.put(CommandEnum.FILTER_NEWS_LIST, new FilterNewsListCommandImpl());
		commandMap.put(CommandEnum.VIEW_NEWS, new ViewNewsCommandImpl());
		commandMap.put(CommandEnum.ADD_COMMENT, new AddCommentCommandImpl());
		commandMap.put(CommandEnum.DEFAULT_COMMAND, new DefaultCommandImpl());
		commandMap.put(CommandEnum.CHANGE_LOCALE, new ChangeLocaleCommand());
	}

	private static class SingletonHolder {
		private final static CommandHelper INSTANCE = new CommandHelper();
	}

	public static CommandHelper getInstance() {
		return SingletonHolder.INSTANCE;
	}

	public ICommand getCommand(String commandName) {
		CommandEnum name = CommandEnum.valueOf(commandName.toUpperCase());
		ICommand command;
		if (null != name) {
			command = commandMap.get(name);
		} else {
			command = commandMap.get(CommandEnum.DEFAULT_COMMAND);
		}
		return command;
	}

}
