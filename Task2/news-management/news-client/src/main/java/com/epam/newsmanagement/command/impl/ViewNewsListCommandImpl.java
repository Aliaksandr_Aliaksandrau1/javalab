package com.epam.newsmanagement.command.impl;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.command.CommandException;
import com.epam.newsmanagement.command.ICommand;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManageService;
import com.epam.newsmanagement.service.impl.NewsManageServiceImpl;

public class ViewNewsListCommandImpl implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;

		ConfigurableApplicationContext ctx = (ClassPathXmlApplicationContext) request.getServletContext().getAttribute("ctx");
		NewsManageService newsManageService = (NewsManageServiceImpl) ctx.getBean("newsManageService");
		
		
		List<NewsTO> newsList = null;
		List<Tag> tagList = null;
		List<Author> authorList = null;
		
		
		
		
	
		
		try {

			newsList = newsManageService.readAllNewsTO();
			tagList = newsManageService.readTags();
			authorList = newsManageService.readAllAuthors();
			System.out.println(tagList);

		} catch (ServiceException e) {
			throw new CommandException (e);
		}
		request.setAttribute("newsList", newsList);
		request.setAttribute("tagList", tagList);
		request.setAttribute("authorList", authorList);

		System.out.println("VIEW_NEWS_LIST");
		page = "/WEB-INF/views/view-news-list.jsp";
	
		
		return page;
	}

}
