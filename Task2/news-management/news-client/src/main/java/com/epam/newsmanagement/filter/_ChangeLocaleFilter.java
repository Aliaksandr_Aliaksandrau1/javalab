package com.epam.newsmanagement.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@WebFilter(urlPatterns = { "/client111" }) // TODO delete filter OR change urlPattern
public class _ChangeLocaleFilter implements Filter {

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		String locale = "locale";
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		
		Object obj = session.getAttribute(locale);
		if (obj==null) {
			session.setAttribute (locale,"EN");
		}
		
		
		chain.doFilter(request, response);
		

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}

}
