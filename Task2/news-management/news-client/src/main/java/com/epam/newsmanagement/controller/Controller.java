package com.epam.newsmanagement.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.newsmanagement.command.CommandException;
import com.epam.newsmanagement.command.CommandHelper;
import com.epam.newsmanagement.command.ICommand;;

//@WebServlet("/client")

@WebServlet(urlPatterns = "/client")

public class Controller extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public Controller() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(processRequest(request));
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(processRequest(request));
		dispatcher.forward(request, response);
		
		/*String page = processRequest(request);
		response.sendRedirect(page);*/
		
		// TODO replace by sendRedirect
	}

	private String processRequest(HttpServletRequest request) throws ServletException, IOException {
		String page = null;
		String commandName = request.getParameter("command");
		System.out.println(commandName);
		ICommand command = CommandHelper.getInstance().getCommand(commandName);

		try {
			page = command.execute(request);

		} catch (CommandException e) {
			page = "/WEB-INF/views/error.jsp";
		}
		return page;

	}

}
