
<!DOCTYPE html>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News Management</title>
</head>
<body>
	<c:import url="/WEB-INF/views/header.jsp" charEncoding="utf-8" />

	
	<br>

	<form method="POST" action="client">
		<input type="hidden" name="command" value="filter_news_list" />
		<select name="authorId">
			<option value="">Please select author</option>
			<c:forEach items="${authorList}" var="author">
				<option value="${author.id}">${author.authorName}</option>
			</c:forEach>
		</select> <label for="tag"> Select tag:</label> 
		<select name="tagId" multiple>
			<option value="">Please select tag</option>
			<c:forEach items="${tagList}" var="tag">
				<option value="${tag.id}">${tag.tagName}</option>
			</c:forEach>
		</select> 
		<input type="submit" value="Filter">

	</form>

	<c:forEach items="${newsList}" var="news">

		<li><c:out value="${news.news.id}" /></li>
		
		<li><p><b><c:out value="${news.news.title}" /> </b>	</p></li>
		
		<li><c:out value="${news.news.shortText}" /></li>
		<li><c:out value="${news.news.creationDate}" /></li>
		<li>comments ${news.commentList.size()}</li>

		<form method="POST" action="client">
			<input type="hidden" name="command" value="view_News" /> <input
				type="hidden" name="newsId" value="${news.news.id}" /> <input
				type="submit" value="view">
		</form>

		<a href="client?command=view_News&newsId=${news.news.id}">View</a>
		<hr />
	</c:forEach>
	<c:import url="/WEB-INF/views/footer.jsp" charEncoding="utf-8" />

</body>
</html>