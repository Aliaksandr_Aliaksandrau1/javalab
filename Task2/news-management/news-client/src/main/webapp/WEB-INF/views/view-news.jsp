<!DOCTYPE html>

<html>
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>News Management</title>
</head>
<body>
	<c:import url="/WEB-INF/views/header.jsp" charEncoding="utf-8" />

	<h2>
		<c:out value="${news.news.title}" />
	</h2>
	<h2>
		<c:out value="${news.news.shortText}" />
	</h2>
	<h2>
		<c:out value="${news.news.fullText}" />
	</h2>
	<h2>
		<c:out value="${news.news.creationDate}" />
	</h2>
	<h2>
		<c:out value="${news.author.authorName}" />
	</h2>


	<table>
		<c:forEach items="${news.commentList}" var="comment">
			<tr>
				<td><c:out value="${comment.creationDate}" /></td>
			</tr>
			<tr>
				<td><c:out value="${comment.commentText}" /></td>
			</tr>
		</c:forEach>
	</table>


	Tags:
	<table>
		<c:forEach items="${news.tagList}" var="tag">
			<tr>
				<td><c:out value="${tag.tagName}" /></td>
			</tr>
		</c:forEach>
	</table>


	<br />

	<form method="POST" action="client">
		<input type="hidden" name="command" value="add_comment" /> <input
			type="hidden" name="newsId" value="${news.news.id}" /> <input
			type="text" name="commentText" /> <input type="submit"
			value="Add comment">
	</form>
	<br>


	<form method="POST" action="client">
		<input type="hidden" name="command" value="view_News" /> <input
			type="hidden" name="newsId" value="${news.news.id+1}" /> <input
			type="submit" value="Next">
	</form>
	<br>
	<a href="client?command=view_News&newsId=${news.news.id+1}">NEXT</a>
	<br>

	<form method="POST" action="client">
		<input type="hidden" name="command" value="view_News" /> <input
			type="hidden" name="newsId" value="${news.news.id-1}" /> <input
			type="submit" value="Prev">
	</form>
	<br>
	<br>
	<a href="client?command=view_News&newsId=${news.news.id-1}">PREVIOUS</a>

	<c:import url="/WEB-INF/views/footer.jsp" charEncoding="utf-8" />
</body>
</html>




