<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">


<title></title>
</head>
<body>

	<h1>Tag page</h1>

	<br />

	<table>
		<c:forEach items="${tagList}" var="tag">
			<tr>
				<td>Tag:</td>
				<td>${tag.tagName}</td>
				<td>edit</td>
			</tr>

		</c:forEach>

	</table>

	<br />

	<sf:form commandName="addTag" modelAttribute="tag" id="tag-form"
		method="POST" action="addTag">
		<sf:input path="tagName" name="tagName" id="tagName" />

		<input type="submit" value="Save" />

	</sf:form>




</body>
</html>




