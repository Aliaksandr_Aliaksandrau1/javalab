<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>


<title></title>
</head>
<body>


	<div class="container-fluid">
		<div class="row">


			<c:forEach items="${authorList}" var="author">
				<div class="col-sm-3">Author:</div>
				<div class="col-sm-6">${author.authorName}</div>
				<div class="col-sm-3">edit</div>

			</c:forEach>
		</div>
	</div>


	<br />



	<sf:form commandName="addAuthor" modelAttribute="author"
		id="author-form" method="POST" action="addAuthor" role="form"
		class="form-inline">

		<div class="form-group">
			<label class="control-label" for="authorName">Author:</label>
			<sf:input path="authorName" name="authorName" id="authorName"
				class="form-control" placeholder="Enter author name" />

		</div>
		<input type="submit" value="Save" class="btn btn-default">
	</sf:form>




</body>
</html>




