<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<title></title>
</head>
<body>


	<div class="container-fluid">


		<h1>add news</h1>

		<div class="row">
			<div class="col-sm-3">Menu</div>
			<div class="col-sm-6">
				<sf:form role="form" class="form-horizontal" commandName="addNews"
					modelAttribute="news" method="POST" action="addNews"
					id="news-add-form">
					<div class="form-group">
						<label class="control-label" for="title">Title:</label>
						 <sf:input path="title"  class="form-control" id="title" name="title"
							placeholder="Enter title"/>
					</div>


					<div class="form-group">
						<label class="control-label" for="creationDate">Date:</label>
						 <sf:input 	type="date" path="creationDate" class="form-control" id="creationDate"
							placeholder="Enter date"/>
					</div>



					<div class="form-group">
						<label class="control-label" for="shortText">Brief:</label>

						<sf:textarea path="shortText" class="form-control" rows="2" id="shortText"
							placeholder="Enter brief"></sf:textarea>
					</div>
					<div class="form-group">
						<label class="control-label" for="fullText">Content:</label>


						<sf:textarea path="fullText" class="form-control" rows="8" id="fullText"
							placeholder="Enter content"></sf:textarea>
					</div>

					<div class="form-group">
						<label for="authorId">Select author:</label> 
						<sf:select path="authorId" class="form-control"  id="authorId">
							 
						<!--  	 <sf:options items="${authorList}" />  -->
							 
							<c:forEach items="${authorList}" var="author">
								<c:if test="${author.expired == null}">
									<sf:option value="${author.id}">${author.authorName}</sf:option>
								</c:if>
							</c:forEach>
							
							
						</sf:select>
				
					</div>
					<div class="form-group">
						<label for="tag"> Select tag:</label>
						 <sf:select  path="tagIdList" multiple="true"
							class="form-control" id="tag">


							<c:forEach items="${tagList}" var="tag">


								<sf:option value="${tag.id}">${tag.tagName}</sf:option>


							</c:forEach>

						</sf:select>
					</div>


					<button type="submit" class="btn btn-default">Save</button>
				</sf:form>
			</div>
			<div class="col-sm-3"></div>
		</div>













	</div>



</body>
</html>




