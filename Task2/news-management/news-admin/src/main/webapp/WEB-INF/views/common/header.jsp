
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
</head>
<body>
	<header>
		<h2>
			<s:message code="general.message.head" text="default text" />
		</h2>
		
		<p>Hello Admin ${pageContext.request.userPrincipal.name} <a href="<c:url value="/logout" />"> Logout</a> </p> 
		<p></p>

		
		<div>
			<a href="?locale=en">English </a> | <a href="?locale=ru">RUS </a>
		</div>

	</header>

</body>
</html>