<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@page session="true"%>
<link href="<c:url value="/resources/css/main.css"/>" rel="stylesheet" type="text/css">

<html>
<body>


	<h1>Login page</h1>

	<c:if test="${not empty error}">
		<div class="error">${error}</div>
	</c:if>
	<c:if test="${not empty msg}">
		<div class="msg">${msg}</div>
	</c:if>


	<form name='loginForm'
		action="<c:url value='/j_spring_security_check' />" method='POST'>

		<table>
			<tr>
				<td>Login:</td>
				<td><input type="text" name="username" value="admin"></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" name="password" value="admin" /></td>
			</tr>
			<tr>
				<td colspan='2'><input name="submit" type="submit"
					value="Login" /></td>
			</tr>
		</table>

		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />

	</form>


</body>
</html>