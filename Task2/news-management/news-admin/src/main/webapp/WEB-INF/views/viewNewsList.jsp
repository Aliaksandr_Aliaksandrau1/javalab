<!DOCTYPE html>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New portal</title>
</head>
<body>
	<!-- modelAttribute="searchCriteria" -->
	
	
	<p id="test">++++++++++++</p>
		
	<c:set var="curPageNumb" value="${41+1}" scope="session"  />
	<c:out value="${sessionScope.curPageNumb}"/>
	
	
	<c:if test="${newsAmount>3}">
	
	<c:forEach begin="1" end="${(newsAmount-newsAmount%2)/2}"  var="val">
     	
			<button type="button" onclick="runScript()" form="search-news-form" value="<c:out value="${val}"/>" class="btn btn-default"><c:out value="${val}"/> </button>
	</c:forEach>
</c:if>
	
	<script> 
	function runScript() {
		document.getElementById("btn_search_news_form").click();
		
		document.getElementById("test").innerHTML="adsasfasfasf";
		
		
	}
	</script>
		
	
	
	<sf:form commandName="filterNewsList" modelAttribute="searchCriteria" id="search-news-form" method="POST"
		action="filterNewsList" role="form" class="form-inline">
		
		<sf:input path="fromRowNum" class="form-control" />
		<sf:input path="toRowNum" class="form-control" />

	
		<div class="form-group">
			<sf:select path="authorId" class="form-control" id="authorId">
				<sf:option value=""> Please select author </sf:option>
				<c:forEach items="${authorList}" var="author">
					<sf:option value="${author.id}">${author.authorName}</sf:option>
				</c:forEach>
			</sf:select>
		</div>
		<div class="form-group">
			<sf:select path="tagIdList" multiple="true" class="form-control"
				id="tag">
				<sf:option value=""> Please select tag </sf:option>
				<c:forEach items="${tagList}" var="tag">
					<sf:option value="${tag.id}">${tag.tagName}</sf:option>
				</c:forEach>
			</sf:select>
		</div>
		<div class="form-group">
			<input type="submit" value="<s:message code="newslist.button.filter"/>" id="btn_search_news_form" class="btn btn-default">
		</div>
	</sf:form>
	<br />
	
	<c:if test="${newsAmount!=null}">
	<c:out value="${newsAmount}"/>
</c:if>
	<c:forEach items="${newsList}" var="news">
		
		<div class="news-box">		
		<a href="viewNews/${news.news.id}"><c:out value="${news.news.title}"/></a>
		<span class="news-author">(by ${news.author.authorName})</span>	
		<span class="news-date">	<c:out value="${news.news.creationDate}"/></span>
				
		<p class="news-text"><c:out value="${news.news.shortText}"/></p>

		<c:forEach items="${news.tagList}" var="tag">
					<span class="news-tag"><c:out value="${tag.tagName}"/> </span> </c:forEach>
		
			<span class="news-comment"> Comments (${news.commentList.size()}) </span>	
		<hr />
		</div>
	</c:forEach>
		


</body>
</html>




