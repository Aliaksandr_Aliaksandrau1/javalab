<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">



<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="author" content="Aliaksandr Aliaksandrau" />
<meta name="company" content="EPAM" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">



<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link href="<c:url value="/resources/css/main.css" />" rel="stylesheet" type="text/css">



<title><tiles:getAsString name="title" /></title>

</head>
<body>

	<div class="container-fluid">
		<tiles:insertAttribute name="header" />
		<div class="row">
			<div class="col-sm-3">
				<tiles:insertAttribute name="menu" />
			</div>
			<div class="col-sm-7">
				<tiles:insertAttribute name="body" />
			</div>
			<div class="col-sm-2"></div>
		
		</div>
		<tiles:insertAttribute name="footer" />
		

	</div>

</body>
</html>