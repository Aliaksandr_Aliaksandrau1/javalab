<!DOCTYPE html>

<html>
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">


<title></title>
</head>
<body>


	<h2><c:out value="${news.news.title}"/></h2>
	<p> (by <c:out value="${news.author.authorName}"/> )</p> 
	<p class="news-date"><c:out value="${news.news.creationDate}"/></p>
	<p class="news-text"><c:out value="${news.news.fullText}"/></p>
		
	
		<c:forEach items="${news.commentList}" var="comment">
		<p>	<c:out value="${comment.creationDate}"/></p>
			<div class="comment-text">	
				<c:out value="${comment.commentText}"/>
			</div>
			
		</c:forEach>


	<br />

	<sf:form commandName="addComment" modelAttribute="comment"
		id="comment-form" method="POST" action="addComment" role="form"
		class="form-inline">

		<div class="form-group">
			<label class="control-label" for="commentText">Add comment:</label>
			<sf:hidden path="newsId" value="${news.news.id}" />
			
		</div>
			<div class="form-group">
						<label class="control-label" for="creationDate">Date:</label>
						 <sf:input 	type="date" path="creationDate" class="form-control" id="creationDate"
							placeholder="Enter date"/>
		</div>
			
			<sf:input path="commentText" name="commentText" id="commentText"
				class="form-control" placeholder="Enter comment" />

		
		<input type="submit" value="Post comment" class="btn btn-default">
	</sf:form>




	
	

</body>
</html>




