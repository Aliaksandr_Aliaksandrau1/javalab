package com.epam.newsmanagement.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManageService;

@Controller
@RequestMapping(value = "/admin")
public class TagManageController {

	private final static Logger LOG = Logger.getRootLogger();

	@Autowired
	private NewsManageService newsManageService;

	@RequestMapping(value = "/viewTag", method = RequestMethod.GET)
	public String addTagForm(Model model) throws ServiceException {
		model.addAttribute("tagList", newsManageService.readTags());
		model.addAttribute(new Tag());
		return "editTag";
	}

	@RequestMapping(value = "/addTag", method = RequestMethod.POST)
	public String addTag(@ModelAttribute("tag") Tag tag, Errors errors, Model model) throws ServiceException {
		if (errors.hasErrors()) {
			return "editTag";
		}
		newsManageService.createTag(tag);
		model.addAttribute("tagList", newsManageService.readTags());
		return "redirect:/viewTag";
	}
	
	@RequestMapping(value = "/tilesTest", method = RequestMethod.GET)
	public String tilesTest(Model model) throws ServiceException {
		model.addAttribute("tagList", newsManageService.readTags());
		model.addAttribute(new Tag());

		return "editTag";
	}
	
	
	
}