package com.epam.newsmanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManageService;

@Controller
@RequestMapping(value = "/admin")
public class AuthorManageController {

	@Autowired
	private NewsManageService newsManageService;

	@RequestMapping(value = "/viewAuthor", method = RequestMethod.GET)
	public String addAuthorForm(Model model) throws ServiceException {
		model.addAttribute("authorList", newsManageService.readAllAuthors());
		model.addAttribute(new Author());
		return "editAuthor";
	}

	@RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
	public String addAuthor(@ModelAttribute("author") Author author, Errors errors, Model model)
			throws ServiceException {
		if (errors.hasErrors()) {
			return "editAuthor";
		}
		newsManageService.createAuthor(author);
		model.addAttribute("authorList", newsManageService.readAllAuthors());

		return "redirect:/viewAuthor";
	}
}