package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManageService;
import com.epam.newsmanagement.utility.NewsVO;
import com.epam.newsmanagement.utility.SearchCriteria;

@Controller
//@Scope("session") // ?
@RequestMapping(value = "/admin")
public class NewsManageController {

	@Autowired
	private NewsManageService newsManageService;

	@RequestMapping(value = "/viewNews/{newsId}", method = RequestMethod.GET)
	public String viewNews(Model model, @PathVariable("newsId") Long newsId) throws ServiceException {

		model.addAttribute("comment", new Comment());
		model.addAttribute("news", newsManageService.readNewsById(newsId));
		return "viewNews";
	}

	@RequestMapping(value = "/viewNewsList", method = RequestMethod.GET)
	public String viewNewsList(Model model) throws ServiceException {
		model.addAttribute("searchCriteria", new SearchCriteria());
		model.addAttribute("newsList", newsManageService.readAllNewsTO());
		model.addAttribute("tagList", newsManageService.readTags());
		model.addAttribute("authorList", newsManageService.readAllAuthors());

		model.addAttribute("newsAmount", newsManageService.countNewsBySearchCriteria(new SearchCriteria()));
		
		return "viewNewsList";
	}

	@RequestMapping(value = "/filterNewsList", method = RequestMethod.POST)
	public String filterNewsList(@ModelAttribute("searchCriteria") SearchCriteria searchCriteria, Errors errors,
			Model model, HttpSession session) throws ServiceException {

		
		//HttpSession session = request.getSession();
		
		Long curPageNumb =  (Long)session.getAttribute("curPageNumb");
		if (curPageNumb == null ||	curPageNumb == 0){
			curPageNumb = new Long(1);
			session.setAttribute("curPageNumb", curPageNumb);
		}
				
		System.out.println(session.getAttribute("curPageNumb"));
		
		
		
		model.addAttribute("searchCriteria", new SearchCriteria());

		model.addAttribute("newsList", newsManageService.readNewsBySearchCriteria(searchCriteria));

		model.addAttribute("tagList", newsManageService.readTags());

		model.addAttribute("authorList", newsManageService.readAllAuthors());

		model.addAttribute("newsAmount", newsManageService.countNewsBySearchCriteria(searchCriteria));

		System.out.println(searchCriteria);

		return "viewNewsList";
	}

	@RequestMapping(value = "/addNewsForm", method = RequestMethod.GET)
	public String addNewsForm(Model model) throws ServiceException {
		model.addAttribute("news", new NewsVO());
		model.addAttribute("tagList", newsManageService.readTags());
		model.addAttribute("authorList", newsManageService.readAllAuthors());
		return "addNews";
	}

	@RequestMapping(value = "/addNews", method = RequestMethod.POST)
	public String addNews(@ModelAttribute("news") NewsVO news, Errors errors, Model model) throws ServiceException {

		NewsTO newsTO = new NewsTO();
		News newsContent = new News();
		newsContent.setTitle(news.getTitle());
		newsContent.setFullText(news.getFullText());
		newsContent.setShortText(news.getShortText());
		newsContent.setCreationDate(news.getCreationDate());
		newsContent.setModificationDate(news.getCreationDate());

		newsTO.setNews(newsContent);

		Author author = new Author();
		author.setId(news.getAuthorId());

		newsTO.setAuthor(author);

		Tag tag;
		List<Tag> tagList = new ArrayList<Tag>();

		for (Long tagId : news.getTagIdList()) {
			tag = new Tag();
			tag.setId(tagId);
			tagList.add(tag);
		}

		newsTO.setTagList(tagList);

		Long newsId = newsManageService.createNews(newsTO);
		model.addAttribute("news", newsManageService.readNewsById(newsId));

		model.addAttribute("comment", new Comment());

		return "viewNews";

	}

}