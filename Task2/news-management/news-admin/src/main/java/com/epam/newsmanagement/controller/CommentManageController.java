package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManageService;

@Controller
@RequestMapping(value = "/admin")
public class CommentManageController {

	private final static Logger LOG = Logger.getRootLogger();

	@Autowired
	private NewsManageService newsManageService;

	@RequestMapping(value = "viewNews/addComment", method = RequestMethod.POST)
	public String addTag(@ModelAttribute("comment") Comment comment, Errors errors, Model model)
			throws ServiceException {

		if (errors.hasErrors()) {
			return "viewNews";
		}

		newsManageService.createComment(comment);
		model.addAttribute("comment", new Comment());
		model.addAttribute("news", newsManageService.readNewsById(comment.getNewsId()));
		return "viewNews";

	}
}