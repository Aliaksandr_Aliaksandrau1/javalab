package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManageService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utility.SearchCriteria;

public class NewsManageServiceImpl implements NewsManageService {
	private NewsService newsService;
	private AuthorService authorService;
	private TagService tagService;
	private CommentService commentService;

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	@Override
	public NewsTO readNewsById(Long id) throws ServiceException {
		NewsTO newsTO = new NewsTO();
		News news = newsService.readNewsById(id);
		Author author = authorService.readAuthorByNewsId(id);
		List<Comment> commentList = commentService.readCommentsByNewsId(id);
		List<Tag> tagList = tagService.readTagsByNewsId(id);

		newsTO.setNews(news);
		newsTO.setAuthor(author);
		newsTO.setCommentList(commentList);
		newsTO.setTagList(tagList);
		return newsTO;
	}

	@Override
	public Long createNews(NewsTO newsTO) throws ServiceException {
		Long newsId = null;

		News news = newsTO.getNews();
		Author author = newsTO.getAuthor();
		List<Tag> tagList = newsTO.getTagList();

		newsId = newsService.createNews(news);
		newsService.setNewsAuthor(newsId, author.getId());

		List<Long> tagIdList = new ArrayList<Long>();
		for (Tag tag : tagList) {
			tagIdList.add(tag.getId());
		}

		tagService.attachTagsToNews(newsId, tagIdList);
		return newsId;
	}

	@Override
	public Boolean updateNews(NewsTO newsTO) throws ServiceException {
		Boolean isUpdated = null;

		News news = newsTO.getNews();
		Long newsId = news.getId();
		Author author = newsTO.getAuthor();
		List<Tag> tagList = newsTO.getTagList();

		isUpdated = newsService.updateNews(news);

		if (!newsService.setNewsAuthor(newsId, author.getId())) {
			isUpdated = false;
		}

		List<Long> tagIdList = new ArrayList<Long>();
		for (Tag tag : tagList) {
			tagIdList.add(tag.getId());
		}

		if (!tagService.attachTagsToNews(newsId, tagIdList)) {
			isUpdated = false;
		}
		return isUpdated;
	}

	@Override
	public Boolean deleteNews(Long newsId) throws ServiceException {
		return newsService.deleteNews(newsId);
	}

	@Override
	public List<NewsTO> readAllNewsTO() throws ServiceException {
		NewsTO newsTO = null;
		List<NewsTO> newsTOList = new ArrayList<NewsTO>();

		List<News> newsList = newsService.readAllNews();

		for (News news : newsList) {
			newsTO = new NewsTO();
			newsTO.setNews(news);
			newsTO.setAuthor(authorService.readAuthorByNewsId(news.getId()));
			newsTO.setTagList(tagService.readTagsByNewsId(news.getId()));
			newsTO.setCommentList(commentService.readCommentsByNewsId(news.getId()));
			newsTOList.add(newsTO);
		}
		
		//System.out.println("   ----------------------- " + newsList);
		
		// TODO rebuild method - one database access

		// return newsService.readAllNewsTO();

		return newsTOList;
	}

	@Override
	public Long createAuthor(Author author) throws ServiceException {
		return authorService.createAuthor(author);
	}

	@Override
	public Boolean setNewsAuthor(Long newsId, Long authorId) throws ServiceException {
		return newsService.setNewsAuthor(newsId, authorId);
	}

	@Override
	public Boolean attachTagsToNews(Long newsId, List<Long> tagIdList) throws ServiceException {

		return tagService.attachTagsToNews(newsId, tagIdList);
	}

	@Override
	public List<Long> createComments(List<Comment> commentList) throws ServiceException {
		return commentService.createComments(commentList);
	}

	@Override
	public Boolean deleteComments(List<Long> commentIdList) throws ServiceException {
		return commentService.deleteCommentsById(commentIdList);
	}

	@Override
	public Long countAllNews() throws ServiceException {
		return newsService.countAllNews();
	}

	@Override
	public Long createTag(Tag tag) throws ServiceException {
		return tagService.createTag(tag);
	}

	@Override
	public Long createComment(Comment comment) throws ServiceException {
		return commentService.createComment(comment);
	}

	@Override
	public List<Tag> readTags() throws ServiceException {
		return tagService.readTags();
	}

	@Override
	public List<Author> readAllAuthors() throws ServiceException {
		return authorService.readAllAuthors();
	}

	@Override
	public List<NewsTO> readNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		NewsTO newsTO = null;
		List<NewsTO> newsTOList = new ArrayList<NewsTO>();

		List<News> newsList = newsService.readNewsBySearchCriteria(searchCriteria);

		for (News news : newsList) {
			newsTO = new NewsTO();
			newsTO.setNews(news);
			newsTO.setAuthor(authorService.readAuthorByNewsId(news.getId()));
			newsTO.setTagList(tagService.readTagsByNewsId(news.getId()));
			newsTO.setCommentList(commentService.readCommentsByNewsId(news.getId()));
			newsTOList.add(newsTO);
		}
		// TODO rebuild method - one database access

		

		return newsTOList;
	}

	@Override
	public Long countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		return newsService.countNewsBySearchCriteria(searchCriteria);
	}

}
