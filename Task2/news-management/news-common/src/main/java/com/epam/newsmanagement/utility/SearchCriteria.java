package com.epam.newsmanagement.utility;

import java.util.List;

public class SearchCriteria {
	private Long authorId;
	private List<Long> tagIdList;
	private Long fromRowNum;
	private Long toRowNum;

	public List<Long> getTagIdList() {
		return tagIdList;
	}

	public void setTagIdList(List<Long> tagIdList) {
		this.tagIdList = tagIdList;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public Long getFromRowNum() {
		return fromRowNum;
	}

	public void setFromRowNum(Long fromRowNum) {
		this.fromRowNum = fromRowNum;
	}

	public Long getToRowNum() {
		return toRowNum;
	}

	public void setToRowNum(Long toRowNum) {
		this.toRowNum = toRowNum;
	}

	@Override
	public String toString() {
		return "SearchCriteria [authorId=" + authorId + ", tagIdList=" + tagIdList + ", fromRowNum=" + fromRowNum
				+ ", toRowNum=" + toRowNum + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((fromRowNum == null) ? 0 : fromRowNum.hashCode());
		result = prime * result + ((tagIdList == null) ? 0 : tagIdList.hashCode());
		result = prime * result + ((toRowNum == null) ? 0 : toRowNum.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchCriteria other = (SearchCriteria) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (fromRowNum == null) {
			if (other.fromRowNum != null)
				return false;
		} else if (!fromRowNum.equals(other.fromRowNum))
			return false;
		if (tagIdList == null) {
			if (other.tagIdList != null)
				return false;
		} else if (!tagIdList.equals(other.tagIdList))
			return false;
		if (toRowNum == null) {
			if (other.toRowNum != null)
				return false;
		} else if (!toRowNum.equals(other.toRowNum))
			return false;
		return true;
	}

	
	
	
}
