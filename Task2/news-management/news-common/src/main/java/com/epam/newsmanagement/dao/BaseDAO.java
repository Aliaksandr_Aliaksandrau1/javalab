package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Entity;
import com.epam.newsmanagement.exception.DAOException;

/**
 * 
 * Represents common DAO operations on entity data from database
 * 
 * @param <T>
 *            - entity class
 */
public interface BaseDAO<T extends Entity> {
	/**
	 * Return Entity from the data source by the identifier of entity
	 * 
	 * @param id
	 *            the identifier of entity
	 * @return Entity
	 * @throws DAOException
	 */
	T readById(Long id) throws DAOException;

	/**
	 * Inserts new entity into the data source
	 * 
	 * @param entity
	 *            entity which must be inserted
	 * @return the identifier of inserted entity
	 * @throws DAOException
	 */
	Long create(T entity) throws DAOException;

	/**
	 * Deletes all data about Entity in the data source
	 * 
	 * @param id
	 *            the identifier of entity
	 * @return true if Entity was deleted
	 * @throws DAOException
	 */
	boolean delete(Long id) throws DAOException;

	/**
	 * Updates data about Entity in the data source
	 * 
	 * @param entity
	 *            the entity to update
	 * @return true if Entity was updated; false otherwise.
	 * @throws DAOException
	 */

	boolean update(T entity) throws DAOException;

}
