package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.utility.DaoUtility;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utility.ExceptionMess;
import com.epam.newsmanagement.utility.NamedConst;

public class CommentDAOImpl implements CommentDAO {
	private static final String COMMENT_FIND_ALL = "SELECT comment_id, news_id, comment_text, creation_date FROM comments";
	private static final String COMMENT_FIND_BY_ID = "SELECT comment_id, news_id, comment_text, creation_date FROM comments WHERE comment_id = ?";
	private static final String COMMENT_CREATE = "INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES (comments_seq.nextval,?,?,?)";
	private static final String COMMENT_UPDATE = "UPDATE comments SET news_id = ?, comment_text = ?, creation_date = ? WHERE comment_id = ?";
	private static final String COMMENT_DELETE = "DELETE FROM comments WHERE comment_id = ?";
	private static final String SQL_COMMENTS_READ_BY_NEWS_ID = "SELECT comment_id, news_id, comment_text, creation_date FROM comments WHERE news_id = ?";

	private final static Logger LOG = Logger.getRootLogger();
	
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Comment readById(Long id) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Comment comment = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(COMMENT_FIND_BY_ID);
			pst.setLong(1, id);
			rs = pst.executeQuery();

			while (rs.next()) {
				comment = new Comment();
				comment.setId(rs.getLong(NamedConst.COMMENT_ID));
				comment.setCommentText(rs.getString(NamedConst.COMMENT_TEXT));
				comment.setNewsId(rs.getLong(NamedConst.NEWS_ID));
				comment.setCreationDate(rs.getDate(NamedConst.COMMENT_CREATION_DATE));
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.COMMENT_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return comment;
	}

	@Override
	public Long create(Comment entity) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Long newCommentId = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(COMMENT_CREATE, new String[] { "comment_id" });
			pst.setLong(1, entity.getNewsId());
			pst.setString(2, entity.getCommentText());
			pst.setTimestamp(3, new java.sql.Timestamp(entity.getCreationDate().getTime()));

			pst.executeUpdate();
			rs = pst.getGeneratedKeys();

			if (rs != null && rs.next()) {
				newCommentId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.COMMENT_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst);
		}
		return newCommentId;
	}

	@Override
	public boolean delete(Long id) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		Boolean isDeleted = false;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(COMMENT_DELETE);
			pst.setLong(1, id);
			if (pst.executeUpdate() == 1) {
				isDeleted = true;
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.COMMENT_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst);
		}

		return isDeleted;
	}

	@Override
	public boolean update(Comment entity) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		boolean isUpdated = false;
		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(COMMENT_UPDATE);
			pst.setLong(1, entity.getNewsId());
			pst.setString(2, entity.getCommentText());
			pst.setTimestamp(3, new java.sql.Timestamp(entity.getCreationDate().getTime()));

			System.out.println("CommentDAO:  " + entity.getCreationDate());

			pst.setLong(4, entity.getId());

			if (pst.executeUpdate() == 1) {
				isUpdated = true;
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.COMMENT_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst);
		}

		return isUpdated;
	}

	public List<Comment> readAll() throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Comment> commentList = null;
		Comment comment = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(COMMENT_FIND_ALL);
			rs = pst.executeQuery();
			commentList = new ArrayList<Comment>();
			while (rs.next()) {
				comment = new Comment();
				comment.setId(rs.getLong(NamedConst.COMMENT_ID));
				comment.setCommentText(rs.getString(NamedConst.COMMENT_TEXT));
				comment.setNewsId(rs.getLong(NamedConst.NEWS_ID));
				comment.setCreationDate(rs.getDate(NamedConst.COMMENT_CREATION_DATE));
				commentList.add(comment);
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.COMMENT_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return commentList;
	}

	@Override
	public List<Comment> readCommentsByNewsId(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Comment> commentList = null;
		Comment comment = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(SQL_COMMENTS_READ_BY_NEWS_ID);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			commentList = new ArrayList<Comment>();
			while (rs.next()) {
				comment = new Comment();
				comment.setId(rs.getLong(NamedConst.COMMENT_ID));
				comment.setCommentText(rs.getString(NamedConst.COMMENT_TEXT));
				comment.setNewsId(rs.getLong(NamedConst.NEWS_ID));
				comment.setCreationDate(rs.getDate(NamedConst.COMMENT_CREATION_DATE));
				commentList.add(comment);
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.COMMENT_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return commentList;
	}

	@Override
	public Boolean deleteCommentsById(List<Long> commentIdList) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		Boolean isDeleted = false;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(COMMENT_DELETE);

			for (Long id : commentIdList) {

				pst.setLong(1, id);
				pst.addBatch();
			}

			pst.executeBatch(); // TODO possibly insert verification
								// if(executeBatch()== ???
			isDeleted = true;

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.COMMENT_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst);
		}

		return isDeleted;
	}

	@Override
	public List<Long> createComments(List<Comment> commentList) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Long newCommentId = null;
		List<Long> newCommentIdList = null;

			// TODO доделать метод
		
		LOG.debug("----------- " + commentList.size());
		
		
		try {
			con = dataSource.getConnection();
			
			
			//pst = con.prepareStatement(COMMENT_CREATE, new String[] { "comment_id" });
			pst = con.prepareStatement(COMMENT_CREATE);
			
			for (Comment entity : commentList) {

				pst.setLong(1, entity.getNewsId());
				pst.setString(2, entity.getCommentText());
				pst.setTimestamp(3, new java.sql.Timestamp(entity.getCreationDate().getTime()));
				pst.addBatch();
			}
			pst.executeBatch();
			
			
			
			//rs = pst.getGeneratedKeys();

			/*newCommentIdList = new ArrayList<Long>();
			
			if (rs != null && rs.next()) {
				newCommentIdList.add(rs.getLong(1));
				
				LOG.debug("============="+rs.getLong(1));
			}*/
		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.COMMENT_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}
		return newCommentIdList;
	}

	
}
