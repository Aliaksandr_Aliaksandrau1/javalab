package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.dao.utility.DaoUtility;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utility.ExceptionMess;
import com.epam.newsmanagement.utility.NamedConst;

public class UserDAOImpl implements UserDAO {
	private final static String USER_READ_BY_ID = "SELECT u.user_id, u.user_name, u.login, u.password, r.role_name FROM users u JOIN roles r ON u.user_id = r.user_id WHERE u.user_id = ?";
	private final static String USER_READ_BY_LOGIN_AND_PASSWORD = "SELECT u.user_id, u.user_name, u.login, u.password, r.role_name FROM users u JOIN roles r ON u.user_id = r.user_id WHERE u.login = ? AND u.password = ?";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public User readById(Long id) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		User user = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(USER_READ_BY_ID);
			pst.setLong(1, id);
			rs = pst.executeQuery();
			List<String> roleList = new ArrayList<String>();
			int counter = 0;
			String role = null;

			while (rs.next()) {

				if (counter < 1) {
					user = new User();
					user.setId(rs.getLong(NamedConst.USER_ID));
					user.setUserName(rs.getString(NamedConst.USER_NAME));
					user.setLogin(rs.getString(NamedConst.USER_LOGIN));
					user.setPassword(rs.getString(NamedConst.USER_PASSWORD));
					counter++;
				}

				role = rs.getString(NamedConst.ROLES_NAME);
				roleList.add(role);

			}
			user.setRoleList(roleList);

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.USER_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return user;
	}

	@Override
	public User readUserByLoginAndPassword(String login, String password) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		User user = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(USER_READ_BY_LOGIN_AND_PASSWORD);
			pst.setString(1, login);
			pst.setString(2, password);
			rs = pst.executeQuery();
			List<String> roleList = new ArrayList<String>();
			int counter = 0;
			String role = null;

			while (rs.next()) {

				if (counter < 1) {
					user = new User();
					user.setId(rs.getLong(NamedConst.USER_ID));
					user.setUserName(rs.getString(NamedConst.USER_NAME));
					user.setLogin(rs.getString(NamedConst.USER_LOGIN));
					user.setPassword(rs.getString(NamedConst.USER_PASSWORD));
					counter++;
				}

				role = rs.getString(NamedConst.ROLES_NAME);
				roleList.add(role);

			}
			user.setRoleList(roleList);

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.USER_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return user;
	}

}
