package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.utility.DaoUtility;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utility.ExceptionMess;
import com.epam.newsmanagement.utility.NamedConst;
import com.epam.newsmanagement.utility.SearchCriteria;

@Component
public class NewsDAOImpl implements NewsDAO {
	private final static Logger LOG = Logger.getRootLogger();

	private static final String SQL_NEWS_READ_BY_ID = "SELECT news_id, title, short_text, full_text, creation_date, modification_date FROM news WHERE news_id = ?";
	private static final String SQL_NEWS_READ_ALL = "SELECT news_id, title, short_text, full_text, creation_date, modification_date FROM news";
	private static final String SQL_NEWS_CREATE = "INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (comments_seq.nextval,?,?,?,?,?)";
	private static final String SQL_NEWS_UPDATE = "UPDATE news SET title = ?, short_text=?, full_text=?, creation_date=?, modification_date=? WHERE news_id = ?";
	private static final String SQL_NEWS_DELETE = "DELETE FROM news WHERE news_id = ?";
	private static final String SQL_SET_NEWS_AUTHOR = "INSERT INTO news_authors (news_id, author_id) VALUES (?, ?)";
	private static final String SQL_COUNT_ALL_NEWS = "SELECT COUNT (news_id) FROM news";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public News readById(Long id) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		News news = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(SQL_NEWS_READ_BY_ID);
			pst.setLong(1, id);
			rs = pst.executeQuery();

			while (rs.next()) {
				news = new News();
				news.setId(rs.getLong(NamedConst.NEWS_ID));
				news.setTitle(rs.getString(NamedConst.NEWS_TITLE));
				news.setShortText(rs.getString(NamedConst.NEWS_SHORT_TEXT));
				news.setFullText(rs.getString(NamedConst.NEWS_FULL_TEXT));
				news.setCreationDate(rs.getDate(NamedConst.NEWS_CREATION_DATE));
				news.setModificationDate(rs.getDate(NamedConst.NEWS_MODIFICATION_DATE));

			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return news;
	}

	@Override
	public Long create(News entity) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Long newNewsId = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(SQL_NEWS_CREATE, new String[] { "news_id" });
			pst.setString(1, entity.getTitle());
			pst.setString(2, entity.getShortText());
			pst.setString(3, entity.getFullText());
			pst.setTimestamp(4, new Timestamp(entity.getCreationDate().getTime()));
			pst.setDate(5, new java.sql.Date(entity.getModificationDate().getTime()));
			pst.executeUpdate();
			rs = pst.getGeneratedKeys();
			if (rs != null && rs.next()) {
				newNewsId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst);
		}
		return newNewsId;
	}

	@Override
	public boolean delete(Long id) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		boolean isDeleted = false;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(SQL_NEWS_DELETE);
			pst.setLong(1, id);
			if (pst.executeUpdate() == 1) {
				isDeleted = true;
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst);
		}

		return isDeleted;
	}

	@Override
	public boolean update(News entity) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		boolean isUpdated = false;
		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(SQL_NEWS_UPDATE);
			pst.setString(1, entity.getTitle());
			pst.setString(2, entity.getShortText());
			pst.setString(3, entity.getFullText());

			pst.setTimestamp(4, new Timestamp(entity.getCreationDate().getTime()));
			pst.setDate(5, new java.sql.Date(entity.getModificationDate().getTime()));
			pst.setLong(6, entity.getId());

			if (pst.executeUpdate() == 1) {
				isUpdated = true;
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst);
		}

		return isUpdated;
	}

	@Override
	public List<News> readAllNews() throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		News news = null;
		List<News> newsList = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(SQL_NEWS_READ_ALL);

			rs = pst.executeQuery();

			newsList = new ArrayList<News>();

			while (rs.next()) {
				news = new News();
				news.setId(rs.getLong(NamedConst.NEWS_ID));
				news.setTitle(rs.getString(NamedConst.NEWS_TITLE));
				news.setShortText(rs.getString(NamedConst.NEWS_SHORT_TEXT));
				news.setFullText(rs.getString(NamedConst.NEWS_FULL_TEXT));
				news.setCreationDate(rs.getDate(NamedConst.NEWS_CREATION_DATE));
				news.setModificationDate(rs.getDate(NamedConst.NEWS_MODIFICATION_DATE));
				newsList.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return newsList;
	}

	@Override
	public boolean setNewsAuthor(Long newsId, Long authorId) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		boolean isSetted = false;
		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(SQL_SET_NEWS_AUTHOR);
			pst.setLong(1, newsId);
			pst.setLong(2, authorId);
			if (pst.executeUpdate() == 1) {
				isSetted = true;
			}
		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst);
		}
		return isSetted;
	}

	@Override
	public Long countAllNews() throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Long newsNumber = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(SQL_COUNT_ALL_NEWS);
			rs = pst.executeQuery();

			while (rs.next()) {
				newsNumber = rs.getLong(1);

			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return newsNumber;
	}

	///////////////////////////////////////

	private static final String NEWS_FIND_BY_ID = "SELECT  n.news_id, n.title, n.short_text, n.full_text, n.creation_date, n.modification_date, t.tag_id, t.tag_name, c.comment_id, c.comment_text, c.creation_date, a.author_id, a.author_name,a.expired FROM News n JOIN news_tags nt ON n.news_id=nt.news_id JOIN tags t ON nt.tag_id=t.tag_id JOIN comments c ON c.news_id=n.news_id JOIN news_authors na ON na.news_id=n.news_id JOIN authors a ON a.author_id=na.author_id WHERE n.news_id = ?";
	private static final String NEWSTO_FIND_ALL = "SELECT  n.news_id, n.title, n.short_text, n.full_text, n.creation_date, n.modification_date, t.tag_id, t.tag_name, c.comment_id, c.comment_text, c.creation_date, a.author_id, a.author_name,a.expired FROM News n FULL JOIN news_tags nt ON n.news_id=nt.news_id FULL JOIN tags t ON nt.tag_id=t.tag_id FULL JOIN comments c ON c.news_id=n.news_id FULL JOIN news_authors na ON na.news_id=n.news_id FULL JOIN authors a ON a.author_id=na.author_id";

	@Override
	public NewsTO readNewsTOById(Long id) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		NewsTO newsTO = null;
		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(NEWS_FIND_BY_ID);
			pst.setLong(1, id);
			rs = pst.executeQuery();
			rs.next();
			newsTO = newsTOBuilder(rs);

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return newsTO;
	}

	public List<NewsTO> readAllNewsTO() throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<NewsTO> newsTOList = null;
		NewsTO newsTO = null;
		Long newsId = null;

		News news = null;

		Map<Long, NewsTO> newsTOMap = new HashMap<Long, NewsTO>();

		Set<Long> newsIdSet = new HashSet<Long>();

		List<Comment> commentList = new ArrayList<Comment>();
		Comment comment = null;
		List<Tag> tagList = new ArrayList<Tag>();
		Tag tag = null;

		Set<Long> commentIdSet = new TreeSet<Long>();
		Set<Long> tagIdSet = new TreeSet<Long>();

		try {

			con = dataSource.getConnection();
			pst = con.prepareStatement(NEWSTO_FIND_ALL);
			rs = pst.executeQuery();
			newsTOList = new ArrayList<NewsTO>();
			while (rs.next()) {
				newsId = rs.getLong(NamedConst.NEWS_ID);
				LOG.debug(newsId + " : newsId");

				if (!newsIdSet.contains(newsId)) {
					if (newsTO != null) {
						newsTOList.add(newsTO);
					}
					// commentIdSet.clear();
					// commentList.clear();
					// tagIdSet.clear();
					// tagList.clear();
					commentIdSet = new TreeSet();
					commentList = new ArrayList<Comment>();
					tagIdSet = new TreeSet();
					tagList = new ArrayList<Tag>();

					newsIdSet.add(newsId);

					newsTO = new NewsTO();
					news = newsBuilder(rs);
					newsTO.setAuthor(authorBuilder(rs));

					comment = commentBuilder(rs);
					commentList.add(comment);
					commentIdSet.add(rs.getLong(NamedConst.COMMENT_ID));

					tag = tagBuilder(rs);
					tagList.add(tag);
					tagIdSet.add(rs.getLong(NamedConst.TAG_ID));

				} else {

					Long commentId = rs.getLong(NamedConst.COMMENT_ID);
					Long tagId = rs.getLong(NamedConst.TAG_ID);

					if (!commentIdSet.contains(commentId)) {

						comment = commentBuilder(rs);
						commentList.add(comment);
						commentIdSet.add(commentId);

					}

					if (!tagIdSet.contains(tagId)) {
						tag = tagBuilder(rs);
						tagList.add(tag);
						tagIdSet.add(tagId);
					}

					newsTO = newsTOBuilder(rs);
					newsTOList.add(newsTO);
				}

			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}
		LOG.debug(newsTOList.size());
		LOG.debug(newsTOList);

		return newsTOList;
	}

	private Comment commentBuilder(ResultSet rs) throws SQLException {
		Comment comment = new Comment();
		comment.setId(rs.getLong(NamedConst.COMMENT_ID));
		comment.setCommentText(rs.getString(NamedConst.COMMENT_TEXT));
		comment.setCreationDate(rs.getDate(NamedConst.COMMENT_CREATION_DATE));
		comment.setNewsId(rs.getLong(NamedConst.NEWS_ID));
		return comment;
	}

	private Tag tagBuilder(ResultSet rs) throws SQLException {
		Tag tag = new Tag();
		tag.setId(rs.getLong(NamedConst.TAG_ID));
		tag.setTagName(rs.getString(NamedConst.TAG_NAME));
		return tag;
	}

	private Author authorBuilder(ResultSet rs) throws SQLException {
		Author author = new Author();
		author.setId(rs.getLong(NamedConst.AUTHOR_ID));
		author.setAuthorName(rs.getString(NamedConst.AUTHOR_NAME));
		author.setExpired(rs.getDate(NamedConst.AUTHOR_EXPIRED));

		return author;

	}

	private News newsBuilder(ResultSet rs) throws SQLException {
		News news = new News();
		news.setId(rs.getLong(NamedConst.NEWS_ID));
		news.setTitle(rs.getString(NamedConst.NEWS_TITLE));
		news.setShortText(rs.getString(NamedConst.NEWS_SHORT_TEXT));
		news.setFullText(rs.getString(NamedConst.NEWS_FULL_TEXT));
		news.setCreationDate(rs.getDate(NamedConst.NEWS_CREATION_DATE));
		news.setModificationDate(rs.getDate(NamedConst.NEWS_MODIFICATION_DATE));
		return news;

	}

	private NewsTO newsTOBuilder(ResultSet rs) throws SQLException {
		NewsTO newsTO = new NewsTO();

		News news = new News();

		// rs.next();

		news.setId(rs.getLong(NamedConst.NEWS_ID));
		news.setTitle(rs.getString(NamedConst.NEWS_TITLE));
		news.setShortText(rs.getString(NamedConst.NEWS_SHORT_TEXT));
		news.setFullText(rs.getString(NamedConst.NEWS_FULL_TEXT));
		news.setCreationDate(rs.getDate(NamedConst.NEWS_CREATION_DATE));
		news.setModificationDate(rs.getDate(NamedConst.NEWS_MODIFICATION_DATE));

		newsTO.setNews(news);
		newsTO.setAuthor(authorBuilder(rs));

		List<Comment> commentList = new ArrayList<Comment>();
		Comment comment = null;
		List<Tag> tagList = new ArrayList<Tag>();
		Tag tag = null;

		Set<Long> commentIdSet = new TreeSet<Long>();
		Set<Long> tagIdSet = new TreeSet<Long>();

		comment = commentBuilder(rs);
		commentList.add(comment);
		commentIdSet.add(rs.getLong(NamedConst.COMMENT_ID));

		tag = tagBuilder(rs);
		tagList.add(tag);
		tagIdSet.add(rs.getLong(NamedConst.TAG_ID));

		while (rs.next()) {

			Long commentId = rs.getLong(NamedConst.COMMENT_ID);
			Long tagId = rs.getLong(NamedConst.TAG_ID);

			if (!commentIdSet.contains(commentId)) {

				comment = commentBuilder(rs);
				commentList.add(comment);
				commentIdSet.add(commentId);

			}

			if (!tagIdSet.contains(tagId)) {
				tag = tagBuilder(rs);
				tagList.add(tag);
				tagIdSet.add(tagId);
			}

		}

		newsTO.setCommentList(commentList);
		newsTO.setTagList(tagList);

		return newsTO;
	}

	@Override
	public List<News> readNewsBySearchCriteria(SearchCriteria searchCriteria) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		News news = null;
		List<News> newsList = null;

		try {
			con = dataSource.getConnection();

			pst = prepStatBuilderBySearchCrit(searchCriteria, con);

			rs = pst.executeQuery();

			newsList = new ArrayList<News>();

			while (rs.next()) {
				news = new News();
				news.setId(rs.getLong(NamedConst.NEWS_ID));
				news.setTitle(rs.getString(NamedConst.NEWS_TITLE));
				news.setShortText(rs.getString(NamedConst.NEWS_SHORT_TEXT));
				// news.setFullText(rs.getString(NamedConst.NEWS_FULL_TEXT));
				news.setCreationDate(rs.getDate(NamedConst.NEWS_CREATION_DATE));
				news.setModificationDate(rs.getDate(NamedConst.NEWS_MODIFICATION_DATE));
				newsList.add(news);

			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return newsList;
	}

	@Override
	public Long countNewsBySearchCriteria(SearchCriteria searchCriteria) throws DAOException {

		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Long newsAmount = null;

		try {
			con = dataSource.getConnection();

			pst = prepStatBuilderForNewsCount(searchCriteria, con);

			rs = pst.executeQuery();

			newsAmount = new Long(0);

			while (rs.next()) {
				newsAmount = rs.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return newsAmount;
	}

	private PreparedStatement prepStatBuilderBySearchCrit(SearchCriteria sC, Connection con) throws DAOException {

		LOG.debug(sC.getAuthorId());
		LOG.debug(sC.getTagIdList());

		StringBuilder sqlQuery = new StringBuilder();
		PreparedStatement pst = null;

		String sqlPart1 = "SELECT NEWS_ID, TITLE, CREATION_DATE, SHORT_TEXT, MODIFICATION_DATE, tag_amount, com_amount FROM "
				+ "(SELECT NEWS_ID, TITLE, CREATION_DATE, SHORT_TEXT, MODIFICATION_DATE, tag_amount, com_amount, rownum rnum FROM "
				+ "(SELECT n.NEWS_ID, n.TITLE, n.CREATION_DATE, n.SHORT_TEXT, n.MODIFICATION_DATE, COUNT(*) tag_amount, nvl(com_amount, 0) com_amount "
				+ "FROM NEWS n ";

		System.out.println("search criteria in newsDAO : " + sC);
		boolean authorLabel = false;
		int aN = 0;
		boolean tagLabel = false;

		Long authorId = sC.getAuthorId();
		List<Long> tagIdList = sC.getTagIdList();
		Long fromRowNum = sC.getFromRowNum();
		Long toRowNum = sC.getToRowNum();

		sqlQuery.append(sqlPart1);

		if (authorId != null) {
			authorLabel = true;
			sqlQuery.append(" JOIN news_authors na ON na.NEWS_ID=n.news_id AND na.AUTHOR_ID=? ");

		}
		if (tagIdList != null && tagIdList.size() > 0) {
			tagLabel = true;
			sqlQuery.append("JOIN NEWS_TAGS nt ON nt.NEWS_ID=n.NEWS_ID AND nt.TAG_ID IN (? ");

			for (int i = 0; i < tagIdList.size() - 1; i++) {
				sqlQuery.append(",?");
			}
			sqlQuery.append(") ");

		}

		sqlQuery.append(
				"LEFT JOIN (SELECT c.news_id, count(*) com_amount from comments c group by c.news_id ) c  ON c.news_id=n.news_id "
						+ "GROUP BY n.NEWS_ID, n.TITLE, n.CREATION_DATE, n.SHORT_TEXT, com_amount, n.MODIFICATION_DATE "
						+ "ORDER BY tag_amount DESC, com_amount DESC, n.MODIFICATION_DATE DESC) ");

		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// TODO implement interval selection
		sqlQuery.append("WHERE ROWNUM<= decode(?,0,rownum,?))WHERE rnum>=?");
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		LOG.debug(sqlQuery);
		int counter = 0;
		try {
			pst = con.prepareStatement(sqlQuery.toString());

			if (authorLabel) {
				pst.setLong(1, sC.getAuthorId());
				aN = 1;

			}

			if (tagLabel) {

				for (int i = 0; i < tagIdList.size(); i++) {
					pst.setLong(i + aN + 1, sC.getTagIdList().get(i));
					counter=i + aN + 1;
				}
			}
		
			//sC.setFromRowNum(new Long(1));
			//sC.setToRowNum(new Long(4));
				
			pst.setLong(counter+1, sC.getToRowNum());
			pst.setLong(counter+2, sC.getToRowNum());
			pst.setLong(counter+3, sC.getFromRowNum());
		
		

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		}
		return pst;
	}

	private PreparedStatement prepStatBuilderForNewsCount(SearchCriteria sC, Connection con) throws DAOException {

		LOG.debug(sC.getAuthorId());
		LOG.debug(sC.getTagIdList());

		String sqlPart1 = "SELECT COUNT(*) news_amount FROM NEWS n ";

		StringBuilder sqlQuery = new StringBuilder(sqlPart1);
		PreparedStatement pst = null;

		boolean authorLabel = false;
		int aN = 0;
		boolean tagLabel = false;

		Long authorId = sC.getAuthorId();
		List<Long> tagIdList = sC.getTagIdList();

		if (authorId != null) {
			authorLabel = true;
			sqlQuery.append(" JOIN news_authors na ON na.NEWS_ID=n.news_id AND na.AUTHOR_ID=? ");

		}
		if (tagIdList != null && tagIdList.size() > 0) {
			tagLabel = true;
			sqlQuery.append("where exists(select 1 from NEWS_TAGS nt where nt.NEWS_ID=n.NEWS_ID AND nt.TAG_ID IN (? ");

			for (int i = 0; i < tagIdList.size() - 1; i++) {
				sqlQuery.append(",?");
			}
			sqlQuery.append(")) ");

		}

		LOG.debug(sqlQuery);
		
		try {
			pst = con.prepareStatement(sqlQuery.toString());

			if (authorLabel) {
				pst.setLong(1, sC.getAuthorId());
				aN = 1;

			}

			if (tagLabel) {

				for (int i = 0; i < tagIdList.size(); i++) {
					pst.setLong(i + aN + 1, sC.getTagIdList().get(i));
					System.out.println(i + aN + 1);
				}
				
			}

			
			
			
		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.NEWS_DAO_EXC + e, e);
		}
		return pst;

	}

}
