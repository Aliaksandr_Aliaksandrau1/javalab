package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides service operations for users
 * 
 */
public interface UserService {
	/**
	 * Return user by the identifier
	 * 
	 * @param id
	 *            the identifier of user
	 * @return user
	 * @throws ServiceException
	 */
	public User readUserById(Long id) throws ServiceException;

	/**
	 * Return user by login and password
	 * 
	 * @param login
	 *            the login of user
	 * @param password
	 *            the password of user
	 * @return user
	 * @throws ServiceException
	 */
	public User readUserByLoginAndPassword(String login, String password) throws ServiceException;

}
