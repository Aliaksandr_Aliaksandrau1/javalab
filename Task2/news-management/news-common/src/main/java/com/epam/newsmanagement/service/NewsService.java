package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.utility.SearchCriteria;

/**
 * Provides service operations for news
 * 
 */
public interface NewsService {
	/**
	 * Returns the news by the identifier.
	 * 
	 * @param id
	 *            the identifier of the news
	 * @return news
	 * @throws ServiceException
	 */
	public News readNewsById(Long id) throws ServiceException;

	/**
	 * Creates a new news.
	 * 
	 * @param news
	 *            the news to insert
	 * @return the identifier of inserted news.
	 * @throws ServiceException
	 */
	public Long createNews(News news) throws ServiceException;

	/**
	 * Updates data about the news.
	 * 
	 * @param news
	 *            the news to update
	 * @return true if the news was updated
	 * @throws ServiceException
	 */
	public boolean updateNews(News news) throws ServiceException;

	/**
	 * Deletes all data about the news.
	 * 
	 * @param id
	 *            the identifier of the news
	 * @return true if tag the news was deleted; false otherwise.
	 * @throws ServiceException
	 */
	public boolean deleteNews(Long id) throws ServiceException;

	/**
	 * Sets the author to the news by identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @param authorId
	 *            the author identifier
	 * @return true if author was attached, false otherwise
	 * @throws ServiceException
	 */
	public boolean setNewsAuthor(Long newsId, Long authorId) throws ServiceException;

	/**
	 * Counts all news.
	 * 
	 * @return the number of all news.
	 * @throws ServiceException
	 */
	public Long countAllNews() throws ServiceException;
	
	public List<NewsTO> readAllNewsTO() throws ServiceException;

	public List<News> readAllNews() throws ServiceException;
	public List<News> readNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;
	public Long countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;
}
