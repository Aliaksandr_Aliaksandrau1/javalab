package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.BaseDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides operations to process data for Authors from the data source
 *
 */
public interface AuthorDAO extends BaseDAO<Author> {
	/**
	 * Receives the author selected by the news identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return author 
	 * 			the author relating the news
	 * 
	 * @throws DAOException
	 */
	public Author readAuthorByNewsId(Long newsId) throws DAOException;
	
	public List<Author> readAllAuthors() throws DAOException;

}
