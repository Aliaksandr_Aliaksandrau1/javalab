package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Provides operations to process data for User from the data source
 *
 */
public interface UserDAO {
	/**
	 * Return user by the identifier
	 * 
	 * @param id
	 *            the identifier of user
	 * @return user
	 * @throws DAOException
	 */
	public User readById(Long id) throws DAOException;

	/**
	 * Returns user with the current login and password
	 * 
	 * @param login
	 *            the user login
	 * @param password
	 *            the user password
	 * @return the user
	 * @throws DAOException
	 */

	public User readUserByLoginAndPassword(String login, String password) throws DAOException;
}
