package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.utility.DaoUtility;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utility.ExceptionMess;
import com.epam.newsmanagement.utility.NamedConst;

@Component
public class TagDAOImpl implements TagDAO {
	private static final String TAG_CREATE = "INSERT INTO tags (tag_id, tag_name) VALUES (tags_seq.nextval,?)";
	private static final String TAG_READ_BY_ID = "SELECT tag_id, tag_name FROM tags WHERE tag_id=?";
	private static final String TAG_DELETE = "DELETE FROM tags WHERE tag_id = ?";
	private static final String TAG_UPDATE = "UPDATE tags SET tag_name = ? WHERE tag_id = ?";
	private static final String SQL_READ_TAGS_BY_NEWS_ID = "SELECT t.tag_id, t.tag_name FROM tags t JOIN news_tags nt ON t.tag_id = nt.tag_id  WHERE nt.news_id = ?";
	private static final String SQL_ATTACH_TAGS_TO_NEWS = "INSERT INTO news_tags (news_id, tag_id) VALUES (?, ?)";

	private static final String SQL_READ_TAGS = "SELECT t.tag_id, t.tag_name FROM tags t";
	
	
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Tag readById(Long id) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Tag tag = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(TAG_READ_BY_ID);
			pst.setLong(1, id);
			rs = pst.executeQuery();

			while (rs.next()) {
				tag = new Tag();
				tag.setId(rs.getLong(NamedConst.TAG_ID));
				tag.setTagName(rs.getString(NamedConst.TAG_NAME));

			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.TAG_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return tag;
	}

	@Override
	public Long create(Tag entity) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Long newTagId = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(TAG_CREATE, new String[] { "tag_id" });

			pst.setString(1, entity.getTagName());
			pst.executeUpdate();
			rs = pst.getGeneratedKeys();
			if (rs != null && rs.next()) {
				newTagId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.TAG_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}
		return newTagId;
	}

	@Override
	public boolean delete(Long id) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean isDeleted = false;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(TAG_DELETE);
			pst.setLong(1, id);
			if (pst.executeUpdate() == 1) {
				isDeleted = true;
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.TAG_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return isDeleted;

	}

	@Override
	public boolean update(Tag entity) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		boolean isUpdated = false;
		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(TAG_UPDATE);
			pst.setString(1, entity.getTagName());
			pst.setLong(2, entity.getId());

			if (pst.executeUpdate() == 1) {
				isUpdated = true;
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.TAG_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst);
		}

		return isUpdated;
	}

	@Override
	public List<Tag> readTagsByNewsId(Long newsId) throws DAOException {

		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Tag> tagList = null;
		Tag tag = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(SQL_READ_TAGS_BY_NEWS_ID);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			tagList = new ArrayList<Tag>();
			while (rs.next()) {
				tag = new Tag();
				tag.setId(rs.getLong(NamedConst.TAG_ID));
				tag.setTagName(rs.getString(NamedConst.TAG_NAME));
				tagList.add(tag);
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.TAG_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return tagList;
	}

	@Override
	public Boolean attachTagsToNews(Long newsId, List<Long> tagIdList) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;

		Boolean isAttached = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(SQL_ATTACH_TAGS_TO_NEWS);

			for (Long tagId : tagIdList) {
				pst.setLong(1, newsId);
				pst.setLong(2, tagId);
				pst.addBatch();
			}

			pst.executeBatch();
			// TODO insert verification
			isAttached = true;

			/*
			 * rs = pst.getGeneratedKeys();
			 * 
			 * 
			 * if (rs != null && rs.next()) { newTagId = rs.getLong(1); }
			 */

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.TAG_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst);
		}

		return isAttached;
	}

	@Override
	public List<Tag> readTags() throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Tag> tagList = null;
		Tag tag = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(SQL_READ_TAGS);
			rs = pst.executeQuery();
			tagList = new ArrayList<Tag>();
			while (rs.next()) {
				tag = new Tag();
				tag.setId(rs.getLong(NamedConst.TAG_ID));
				tag.setTagName(rs.getString(NamedConst.TAG_NAME));
				tagList.add(tag);
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.TAG_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return tagList;
	}
}
