package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.utility.ExceptionMess;
import com.epam.newsmanagement.utility.SearchCriteria;

@Service
public class NewsServiceImpl implements NewsService {

	private NewsDAO newsDAO;

	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	@Override
	public News readNewsById(Long id) throws ServiceException {
		try {

			return newsDAO.readById(id);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Long createNews(News news) throws ServiceException {
		try {
			return newsDAO.create(news);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean updateNews(News news) throws ServiceException {
		try {
			return newsDAO.update(news);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean deleteNews(Long id) throws ServiceException {
		try {
			return newsDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean setNewsAuthor(Long newsId, Long authorId) throws ServiceException {
		try {
			return newsDAO.setNewsAuthor(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Long countAllNews() throws ServiceException {
		try {
			return newsDAO.countAllNews();
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public List<NewsTO> readAllNewsTO() throws ServiceException {
		try {
			return newsDAO.readAllNewsTO();
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public List<News> readAllNews() throws ServiceException {
		try {
			return newsDAO.readAllNews();
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public List<News> readNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.readNewsBySearchCriteria(searchCriteria);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Long countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.countNewsBySearchCriteria(searchCriteria);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.NEWS_SERVICE_EXC + e, e);
		}
	}
	

}
