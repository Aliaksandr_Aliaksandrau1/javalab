package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Provides operations to process data for Comments from the data source
 *
 */
public interface CommentDAO extends BaseDAO<Comment> {
	/**
	 * Receives the list of comments
	 * 
	 * @return the list of comments
	 * @throws DAOException
	 */
	public List<Comment> readAll() throws DAOException;

	/**
	 * Receives the list of comments selected by the news identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return the list of comments
	 * @throws DAOException
	 */
	public List<Comment> readCommentsByNewsId(Long newsId) throws DAOException;

	/**
	 * Deletes data about Comments in the data source
	 * 
	 * @param commentIdList
	 *            the list of the comments identifiers
	 * @return true if all comments were deleted
	 * @throws DAOException
	 */
	public Boolean deleteCommentsById(List<Long> commentIdList) throws DAOException;

	/**
	 * Inserts new Comments into the data source
	 * 
	 * @param commentList
	 *            list of comments which must be inserted
	 * @return the identifiers list of inserted comments
	 * @throws DAOException
	 */
	public List<Long> createComments(List<Comment> commentList) throws DAOException; // TODO delete method
	
	
}
