package com.epam.newsmanagement.utility;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtility {

	public static void main(String[] args) {

		fromDateToTimestamp(new Date());

		System.out.println(dateFromString("2016-12-21 15:34:17"));

	}

	public static Timestamp fromDateToTimestamp(Date date) {
		Timestamp ts = null;
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date1;
		try {
			date1 = dateFormat.parse("23/09/2007");
			long time = date1.getTime();
			ts = new Timestamp(time);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ts;
	}

	public static java.util.Date dateFromString(String str) {
		// TODO refactor method
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		try {
			date = formatter.parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

}
