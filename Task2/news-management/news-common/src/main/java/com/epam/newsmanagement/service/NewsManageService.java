package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.utility.SearchCriteria;

public interface NewsManageService {
	public Long createNews(NewsTO newsTO) throws ServiceException;

	public Boolean updateNews(NewsTO newsTO) throws ServiceException;

	public Boolean deleteNews(Long newsId) throws ServiceException;

	public List<NewsTO> readAllNewsTO() throws ServiceException;

	public Long createAuthor(Author author) throws ServiceException;

	public Long createTag(Tag tag) throws ServiceException;

	public Long createComment(Comment comment) throws ServiceException;

	public Boolean setNewsAuthor(Long newsId, Long authorId) throws ServiceException;

	public List<NewsTO> readNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

	public Long countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;
	
	public NewsTO readNewsById(Long newsId) throws ServiceException;

	public Boolean attachTagsToNews(Long newsId, List<Long> tagIdList) throws ServiceException;

	public List<Long> createComments(List<Comment> commentList) throws ServiceException;

	public Boolean deleteComments(List<Long> commentIdList) throws ServiceException;

	public Long countAllNews() throws ServiceException;

	public List<Tag> readTags() throws ServiceException;

	public List<Author> readAllAuthors() throws ServiceException;

}
