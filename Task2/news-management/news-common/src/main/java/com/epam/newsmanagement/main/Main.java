package com.epam.newsmanagement.main;

import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.impl.NewsDAOImpl;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.service.NewsManageService;
import com.epam.newsmanagement.service.impl.NewsManageServiceImpl;

public class Main {
	private final static Logger logger = Logger.getRootLogger();

	public static void main(String[] args) {
		test();
	}

	private static void test() {

		ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("app-context-bean.xml");
		

		Locale.setDefault(Locale.ENGLISH);

		//NewsDAOImpl nDAO = (NewsDAOImpl) ctx.getBean("newsDAO");
		
		NewsManageService nmService = (NewsManageServiceImpl) ctx.getBean("newsManageService");

		try {
		
			
			NewsTO newsTO = nmService.readNewsById(new Long(1));
//			System.out.println(newsTO);
			
			//nDAO.setNewsAuthor(new Long(1), new Long (1));
			
		NewsDAO nDAO = (NewsDAOImpl) ctx.getBean("newsDAO");
		NewsTO newsTO2 = nDAO.readNewsTOById(new Long(2));
			
		
		System.out.println("_____" + newsTO2);
		
		List<NewsTO> newsTOList = nDAO.readAllNewsTO();
		System.out.println(newsTOList.size());
		for(NewsTO newsTOvar: newsTOList) {
			System.out.println(newsTOvar);
			System.out.println("-------------------------------------------------------");
		}
		
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ctx.close();
	}
	
}