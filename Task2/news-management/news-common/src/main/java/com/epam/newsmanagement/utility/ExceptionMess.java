package com.epam.newsmanagement.utility;

public class ExceptionMess {
	public static final String COMMENT_DAO_EXC = "Exception in CommentDAOImpl : ";
	public static final String TAG_DAO_EXC = "Exception in TagDAOImpl : ";
	public static final String AUTHOR_DAO_EXC = "Exception in AuthorDAOImpl : ";
	public static final String NEWS_DAO_EXC = "Exception in NewsDAOImpl : ";
	public static final String USER_DAO_EXC = "Exception in UserDAOImpl : ";

	public static final String COMMENT_SERVICE_EXC = "Exception in CommentServiceImpl : ";
	public static final String TAG_SERVICE_EXC = "Exception in TagServiceImpl : ";
	public static final String AUTHOR_SERVICE_EXC = "Exception in AuthorServiceImpl : ";
	public static final String NEWS_SERVICE_EXC = "Exception in NewsServiceImpl : ";
	public static final String USER_SERVICE_EXC = "Exception in UserServiceImpl : ";
}
