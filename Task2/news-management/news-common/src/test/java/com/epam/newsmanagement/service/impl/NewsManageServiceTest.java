package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsTO;

@RunWith(MockitoJUnitRunner.class)
public class NewsManageServiceTest {

	@Mock
	private NewsDAO newsDAO;
	private NewsServiceImpl newsService;
	private News news = mock(News.class);
	private NewsTO newsTO = mock(NewsTO.class);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		newsService = new NewsServiceImpl();
		newsService.setNewsDAO(newsDAO);

		news.setId(1L);
		newsTO.setNews(news);

		// author.setId(1L);
		// news.setAuthor(new Author());
		// news.setCommentList(new ArrayList<Comment>());
		// TODO add other data in setters

	}

	@Test
	public void testReadNewsById() throws Exception {
		newsService.readNewsById(news.getId());
		verify(newsDAO).readById(news.getId());

	}

}
