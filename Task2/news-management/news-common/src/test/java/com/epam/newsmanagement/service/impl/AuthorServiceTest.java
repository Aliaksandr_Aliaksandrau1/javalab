package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.utility.DateUtility;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {
	@Mock
	private AuthorDAO authorDAO;
	private AuthorServiceImpl authorService;
	private Author author = mock(Author.class);
	private News news = mock(News.class);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		authorService = new AuthorServiceImpl();
		authorService.setAuthorDAO(authorDAO);
		author.setId(1L);
		author.setAuthorName("author name");
		author.setExpired(DateUtility.dateFromString("2016-02-26 00:00:00"));
		news.setId(1L);
		
	}

	@Test
	public void readAuthorByIdTest() throws Exception {
		authorService.readAuthorById(author.getId());
		verify(authorDAO).readById(author.getId());

	}

	@Test
	public void createTagTest() throws Exception {
		authorService.createAuthor(author);
		verify(authorDAO).create(author);

	}

	@Test
	public void updateTagTest() throws Exception {
		authorService.updateAuthor(author);
		verify(authorDAO).update(author);

	}

	@Test
	public void deleteTagTest() throws Exception {
		authorService.deleteAuthor(author.getId());
		verify(authorDAO).delete(author.getId());

	}
	
	@Test
	public void readAuthorByNewsIdTest() throws Exception {
		authorService.readAuthorByNewsId(news.getId());
		verify(authorDAO).readAuthorByNewsId(news.getId());

	}
}
