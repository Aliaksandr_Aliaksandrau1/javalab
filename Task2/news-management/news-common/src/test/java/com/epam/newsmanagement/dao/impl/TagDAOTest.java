package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/dbunit_context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })

@DatabaseSetup(value = "/TagDAOTest_data.xml", type = DatabaseOperation.CLEAN_INSERT)

public class TagDAOTest {

	@Autowired
	private TagDAO tagDAO;

	@Test
	public void testCreate() throws Exception {
		Long tagId = null;
		Tag tagAct = tagBuilder(tagId, "tag name test");
		tagId = tagDAO.create(tagAct);
		tagAct.setId(tagId);
		Tag tagExp = tagDAO.readById(tagId);
		assertTag(tagExp, tagAct);

	}

	@Test
	public void testDelete() throws Exception {

		Long id = new Long(3);
		tagDAO.delete(id);
		Tag tagAct = tagDAO.readById(id);
		Assert.assertNull(tagAct);

	}

	@Test
	public void testUpdate() throws Exception {

		Long id = new Long(3);
		Tag tagExp = tagBuilder(id, "tag text update");
		tagDAO.update(tagExp);
		Tag tagAct = tagDAO.readById(id);
		assertTag(tagExp, tagAct);

	}

	@Test
	public void testReadByID() throws Exception {
		Long id = new Long(1);
		Tag tagExp = tagBuilder(id, "tag name 1");
		Tag tagAct = tagDAO.readById(id);
		assertTag(tagExp, tagAct);

	}

	@Test
	public void testReadTagsByNewsId() throws Exception {
		Long newsId = new Long(1);
		List<Tag> tagListExp = new ArrayList<Tag>();
		Tag tagExp1 = tagBuilder(new Long(1), "tag name 1");
		Tag tagExp2 = tagBuilder(new Long(2), "tag name 2");
		tagListExp.add(tagExp1);
		tagListExp.add(tagExp2);
		List<Tag> tagListAct = tagDAO.readTagsByNewsId(newsId);
		assertTagList(tagListExp, tagListAct);
	}

	
	@Test
	public void testAttachTagsToNews() throws Exception {
		List<Tag> tagListAct = null;
		List<Long> tagIdList = new ArrayList<Long>();

		Long newsId = new Long(2);
		Long tagId1 = new Long(2);
		Long tagId2 = new Long(3);
		tagIdList.add(tagId1);
		tagIdList.add(tagId2);
		Boolean result = tagDAO.attachTagsToNews(newsId, tagIdList);

		Tag tag1Act = tagDAO.readById(tagId1);
		Tag tag2Act = tagDAO.readById(tagId2);

		tagListAct = tagDAO.readTagsByNewsId(newsId);

		Assert.assertTrue(tagListAct.contains(tag1Act));
		Assert.assertTrue(tagListAct.contains(tag2Act));
		Assert.assertTrue(result);

	}

	private Tag tagBuilder(Long id, String tagName) {
		Tag tag = new Tag();
		tag.setId(id);
		tag.setTagName(tagName);
		return tag;
	}

	private void assertTagList(List<Tag> tagListExp, List<Tag> tagListAct) {
		Assert.assertNotNull(tagListAct);
		Assert.assertEquals(tagListExp.size(), tagListAct.size());
		for (int i = 0; i < tagListExp.size(); i++) {
			assertTag(tagListExp.get(i), tagListAct.get(i));
		}
	}

	private void assertTag(Tag tagExp, Tag tagAct) {
		Assert.assertNotNull(tagAct);
		Assert.assertEquals(tagExp.getId(), tagAct.getId());
		Assert.assertEquals(tagExp.getTagName(), tagAct.getTagName());

		Assert.assertEquals(tagExp, tagAct);
	}

}
