package com.epam.newsmanagement.dao.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.utility.DateUtility;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/dbunit_context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })

@DatabaseSetup(value = { "/AuthorDAOTest_data.xml" }, type = DatabaseOperation.CLEAN_INSERT)
public class AuthorDAOTest {

	@Autowired
	private AuthorDAO authorDAO;

	@Test
	public void testReadById() throws Exception {
		Long id = new Long(1);
		Author authorExp = authorBuilder(id, "Платон", "2015-06-15 00:00:00");
		Author authorAct = authorDAO.readById(id);
		assertAuthor(authorExp, authorAct);
	}

	@Test
	public void testCreate() throws Exception {
		Long id = null;
		Author authorExp = authorBuilder(id, "Ф. М. Достоевский", "2016-02-26 00:00:00");
		id = authorDAO.create(authorExp);
		authorExp.setId(id);
		Author authorAct = authorDAO.readById(id);
		assertAuthor(authorExp, authorAct);

	}

	@Test
	public void testUpdate() throws Exception {
		Long id = new Long(1);
		Author authorExp = authorBuilder(id, "W. Shakespeare", "2016-02-26 00:00:00");
		authorDAO.update(authorExp);
		Author authorAct = authorDAO.readById(id);
		assertAuthor(authorExp, authorAct);

	}

	@Test
	public void testDelete() throws Exception {
		Long id = new Long(2);
		authorDAO.delete(id);
		Author authorAct = authorDAO.readById(id);
		Assert.assertNull(authorAct);
	}

	@Test
	public void testReadAuthorByNewsId() throws Exception {
		Long newsId = new Long(1);
		Long authorsId = new Long(1);
		Author authorExp = authorBuilder(authorsId, "Платон", "2015-06-15 00:00:00");
		Author authorAct = authorDAO.readAuthorByNewsId(newsId);
		assertAuthor(authorExp, authorAct);

	}

	private Author authorBuilder(Long authorId, String authorName, String expired) {
		Author author = new Author();
		author.setId(authorId);
		author.setAuthorName(authorName);
		author.setExpired(DateUtility.dateFromString(expired));
		return author;
	}

	private void assertAuthor(Author authorExp, Author authorAct) {
		Assert.assertNotNull(authorAct);
		Assert.assertEquals(authorExp.getId(), authorAct.getId());
		Assert.assertEquals(authorExp.getAuthorName(), authorAct.getAuthorName());
		Assert.assertEquals(authorExp.getExpired(), authorAct.getExpired());
		Assert.assertEquals(authorExp, authorAct);

	}

}
