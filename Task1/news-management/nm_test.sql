-- Clean up from any previous tutorial actions


DROP TABLE comments;
DROP TABLE news_authors;
DROP TABLE news_tags;
DROP TABLE tags;
DROP TABLE news;
DROP TABLE authors;
DROP TABLE roles;
DROP TABLE users;

DROP SEQUENCE users_seq;
DROP SEQUENCE news_seq;
DROP SEQUENCE comments_seq;
DROP SEQUENCE tags_seq;
DROP SEQUENCE authors_seq;

-- Create test tables

CREATE TABLE news (
	news_id NUMBER(20) NOT NULL,
	title VARCHAR2(30) NOT NULL,
	short_text VARCHAR2(100) NOT NULL,
	full_text VARCHAR2 (2000) NOT NULL,
	creation_date TIMESTAMP NOT NULL,
	modification_date DATE NOT NULL,
	PRIMARY KEY(news_id)
	
);

CREATE SEQUENCE news_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE;


CREATE TABLE comments (
	comment_id NUMBER(20) NOT NULL,
	news_id NUMBER(20) NOT NULL,
	comment_text VARCHAR2(100) NOT NULL,
	creation_date TIMESTAMP NOT NULL,
	PRIMARY KEY(comment_id)
  --,
  --CONSTRAINT news_id_fk foreign key (news_id) references news
);

CREATE SEQUENCE comments_seq
START WITH 10 
INCREMENT BY 1 
NOMAXVALUE;


CREATE TABLE authors (
	author_id NUMBER(20) NOT NULL,
	author_name VARCHAR2(30) NOT NULL,
	expired TIMESTAMP,
	PRIMARY KEY(author_id)
);

CREATE SEQUENCE authors_seq
START WITH 10 
INCREMENT BY 1 
NOMAXVALUE;


CREATE TABLE news_authors (
	news_id NUMBER (20) NOT NULL,
	author_id NUMBER(20) NOT NULL,
	CONSTRAINT na_news_id_fk foreign key (news_id) references news,
	CONSTRAINT na_authors_id_fk foreign key (author_id) references authors
);



CREATE TABLE tags (
	tag_id NUMBER (20) NOT NULL,
    tag_name VARCHAR2 (30) NOT NULL,
  PRIMARY KEY(tag_id)
);


CREATE SEQUENCE tags_seq
START WITH 10 
INCREMENT BY 1 
NOMAXVALUE;









CREATE TABLE news_tags (
	news_id NUMBER (20) NOT NULL,
	tag_id NUMBER(20) NOT NULL,
	CONSTRAINT nt_news_id_fk foreign key (news_id) references news,
	CONSTRAINT nt_tags_id_fk foreign key (tag_id) references tags
);

CREATE TABLE users (
	user_id NUMBER (20) NOT NULL,
	user_name VARCHAR2(50) NOT NULL,
	login VARCHAR2(30) NOT NULL,
	password VARCHAR2(30) NOT NULL,
	PRIMARY KEY(user_id)
);

CREATE SEQUENCE users_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE;


CREATE TABLE roles (
	user_id NUMBER (20) NOT NULL,
	role_name VARCHAR2 (50) NOT NULL,
	CONSTRAINT r_users_id_fk foreign key (user_id) references users
);

commit;
