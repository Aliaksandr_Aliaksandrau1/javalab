package com.epam.newsmanagement.exception;

public class NewsManagProjectException extends Exception {
	private static final long serialVersionUID = -3828262673971016179L;

	public NewsManagProjectException() {

	}

	public NewsManagProjectException(String message) {
		super(message);
	}

	public NewsManagProjectException(Throwable cause) {
		super(cause);
	}

	public NewsManagProjectException(String message, Throwable cause) {
		super(message, cause);
	}
}
