package com.epam.newsmanagement.dao.utility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class DaoUtility {

	private final static Logger LOG = Logger.getRootLogger();

	public static void closeCon(Connection con, PreparedStatement pst, ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				LOG.log(Level.ERROR, "ResultSet isn't closed.");
			}
		}

		closeCon(con, pst);

	}

	public static void closeCon(Connection con, PreparedStatement pst) {

		if (pst != null) {
			try {
				pst.close();
			} catch (SQLException e) {
				LOG.log(Level.ERROR, "PreparedStatement isn't closed.");
			}
		}

		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				LOG.log(Level.ERROR, "Connection isn't return to the pool.");
			}
		}
	}

}
