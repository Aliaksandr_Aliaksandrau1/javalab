package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Provides operations to process data for News from the data source
 *
 */
public interface NewsDAO extends BaseDAO<News> {
	/**
	 * Sets the author to the news by identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @param authorId
	 *            the author identifier
	 * @return true if author was attached, false otherwise
	 * @throws DAOException
	 */
	public boolean setNewsAuthor(Long newsId, Long authorId) throws DAOException;

	/**
	 * Counts all news in the data source
	 * 
	 * 
	 * @return the number of all news in the data source
	 * @throws DAOException
	 */

	public Long countAllNews() throws DAOException;

	//////////////////

	public NewsTO readNewsTOById(Long id) throws DAOException;

	public List<NewsTO> readAllNewsTO() throws DAOException;

}
