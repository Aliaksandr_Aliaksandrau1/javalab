package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.UserService;
import com.epam.newsmanagement.utility.ExceptionMess;

public class UserServiceImpl implements UserService {

	private UserDAO userDAO;

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public User readUserById(Long id) throws ServiceException {
		try {

			return userDAO.readById(id);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.USER_SERVICE_EXC + e, e);
		}
	}

	@Override
	public User readUserByLoginAndPassword(String login, String password) throws ServiceException {
		try {

			return userDAO.readUserByLoginAndPassword(login, password);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.USER_SERVICE_EXC + e, e);
		}
	}

}
