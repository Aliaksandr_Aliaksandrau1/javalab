package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Provides operations to process data for Tags from the data source
 *
 */
public interface TagDAO extends BaseDAO<Tag> {
	/**
	 * Receives the list of tags selected by the news identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return the list of tags
	 * @throws DAOException
	 */
	public List<Tag> readTagsByNewsId(Long newsId) throws DAOException;

	/**
	 * Attaches the list of tags to the news by identifiers
	 * 
	 * @param newsId
	 *            the news identifier
	 * @param tagIdList
	 *            list of tag identifiers which should be attached to the news
	 * @return true if tags were attached, false otherwise
	 * @throws DAOException
	 */

	public Boolean attachTagsToNews(Long newsId, List<Long> tagIdList) throws DAOException;
}
