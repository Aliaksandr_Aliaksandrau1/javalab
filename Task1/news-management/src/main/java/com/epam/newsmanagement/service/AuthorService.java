package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides service operations for authors
 * 
 */
public interface AuthorService {
	/**
	 * Returns the author by the identifier
	 * 
	 * @param id
	 *            the identifier of author
	 * @return author
	 * @throws ServiceException
	 */
	public Author readAuthorById(Long id) throws ServiceException;

	/**
	 * Creates a new Author
	 * 
	 * @param author
	 *            author which must be inserted
	 * @return the identifier of inserted author
	 * @throws ServiceException
	 */
	public Long createAuthor(Author author) throws ServiceException;

	/**
	 * Updates data about Author
	 * 
	 * @param author
	 *            the author to update
	 * @return true if the author was updated
	 * @throws ServiceException
	 */
	public boolean updateAuthor(Author author) throws ServiceException;

	/**
	 * Deletes all data about the author
	 * 
	 * @param id
	 *            the identifier of author
	 * @return true if author was deleted
	 * @throws ServiceException
	 */
	public boolean deleteAuthor(Long id) throws ServiceException;

	/**
	 * Receives the author selected by the news identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return author the author relating the news
	 * 
	 * @throws ServiceException
	 */
	public Author readAuthorByNewsId(Long newsId) throws ServiceException;
}
