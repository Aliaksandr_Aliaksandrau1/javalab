package com.epam.newsmanagement.utility;

public class NamedConst {
	public static final String COMMENT_ID = "comment_id";
	public static final String COMMENT_TEXT = "comment_text";
	public static final String COMMENT_CREATION_DATE = "creation_date";
	
	
	public static final String AUTHOR_ID = "author_id";
	public static final String AUTHOR_NAME = "author_name";
	public static final String AUTHOR_EXPIRED = "expired";
	
	public static final String TAG_ID = "tag_id";
	public static final String TAG_NAME = "tag_name";
	
	public static final String NEWS_ID = "news_id";
	public static final String NEWS_TITLE = "title";
	public static final String NEWS_SHORT_TEXT = "short_text";
	public static final String NEWS_FULL_TEXT = "full_text";
	public static final String NEWS_CREATION_DATE = "creation_date";
	public static final String NEWS_MODIFICATION_DATE = "modification_date";
	
	
	public static final String USER_ID = "user_id";
	public static final String USER_NAME = "user_name";
	public static final String USER_LOGIN = "login";
	public static final String USER_PASSWORD = "password";
	
	public static final String ROLES_NAME = "role_name";
	
	
	
	
}
