package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;

public interface NewsManageService {
	public Long createNews(NewsTO newsTO) throws ServiceException;

	public Boolean updateNews(NewsTO newsTO) throws ServiceException;

	public Boolean deleteNews(Long newsId) throws ServiceException;

	public List<NewsTO> readAllNews() throws ServiceException;

	public Long createAuthor(Author author) throws ServiceException;

	public Boolean setNewsAuthor(Long newsId, Long authorId) throws ServiceException;
	
	// public List<NewsTo> readNewsBySearcCriteria(SearchCriteria ??) throws
	// ServiceException;

	public NewsTO readNewsById(Long newsId) throws ServiceException;

	public Boolean attachTagsToNews (Long newsId, List<Long> tagIdList) throws ServiceException;

	public List<Long> createComments (List<Comment> commentList) throws ServiceException;

	public Boolean deleteComments(List<Long> commentIdList) throws ServiceException;

	public Long countAllNews() throws ServiceException;

}
