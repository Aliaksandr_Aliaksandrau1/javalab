package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManageService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

public class NewsManageServiceImpl implements NewsManageService {
	private NewsService newsService;
	private AuthorService authorService;
	private TagService tagService;
	private CommentService commentService;

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	@Override
	public NewsTO readNewsById(Long id) throws ServiceException {
		NewsTO newsTO = new NewsTO();
		News news = newsService.readNewsById(id);
		Author author = authorService.readAuthorByNewsId(id);
		List<Comment> commentList = commentService.readCommentsByNewsId(id);
		List<Tag> tagList = tagService.readTagsByNewsId(id);

		newsTO.setNews(news);
		newsTO.setAuthor(author);
		newsTO.setCommentList(commentList);
		newsTO.setTagList(tagList);
		return newsTO;
	}

	@Override
	public Long createNews(NewsTO newsTO) throws ServiceException {
		Long newsId = null;

		News news = newsTO.getNews();
		Author author = newsTO.getAuthor();
		List<Tag> tagList = newsTO.getTagList();

		newsId = newsService.createNews(news);
		newsService.setNewsAuthor(newsId, author.getId());

		List<Long> tagIdList = new ArrayList<Long>();
		for (Tag tag : tagList) {
			tagIdList.add(tag.getId());
		}

		tagService.attachTagsToNews(newsId, tagIdList);
		return newsId;
	}

	@Override
	public Boolean updateNews(NewsTO newsTO) throws ServiceException {
		Boolean isUpdated = null;

		News news = newsTO.getNews();
		Long newsId = news.getId();
		Author author = newsTO.getAuthor();
		List<Tag> tagList = newsTO.getTagList();

		isUpdated = newsService.updateNews(news);

		if (!newsService.setNewsAuthor(newsId, author.getId())) {
			isUpdated = false;
		}

		List<Long> tagIdList = new ArrayList<Long>();
		for (Tag tag : tagList) {
			tagIdList.add(tag.getId());
		}

		if (!tagService.attachTagsToNews(newsId, tagIdList)) {
			isUpdated = false;
		}
		return isUpdated;
	}

	@Override
	public Boolean deleteNews(Long newsId) throws ServiceException {
		return newsService.deleteNews(newsId);
	}

	@Override
	public List<NewsTO> readAllNews() throws ServiceException {
		List<NewsTO> newsTOList = new ArrayList<NewsTO>();

		// TODO remake method

		return newsTOList;
	}

	@Override
	public Long createAuthor(Author author) throws ServiceException {
		return authorService.createAuthor(author);
	}

	@Override
	public Boolean setNewsAuthor(Long newsId, Long authorId) throws ServiceException {
		return newsService.setNewsAuthor(newsId, authorId);
	}

	@Override
	public Boolean attachTagsToNews(Long newsId, List<Long> tagIdList) throws ServiceException {

		return tagService.attachTagsToNews(newsId, tagIdList);
	}

	@Override
	public List<Long> createComments(List<Comment> commentList) throws ServiceException {
		return commentService.createComments(commentList);
	}

	@Override
	public Boolean deleteComments(List<Long> commentIdList) throws ServiceException {
		return commentService.deleteCommentsById(commentIdList);
	}

	@Override
	public Long countAllNews() throws ServiceException {
		return newsService.countAllNews();
	}

}
