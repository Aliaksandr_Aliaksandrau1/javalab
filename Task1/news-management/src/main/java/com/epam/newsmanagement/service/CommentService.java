package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides service operations for comments
 * 
 */
public interface CommentService {
	/**
	 * Receives the list of comments
	 * 
	 * @return the list of comments
	 * @throws ServiceException
	 */
	public List<Comment> readAllComments() throws ServiceException;

	/**
	 * Returns comment by the identifier
	 * 
	 * @param id
	 *            the identifier of comment
	 * @return comment
	 * @throws ServiceException
	 */
	public Comment readCommentById(Long id) throws ServiceException;

	/**
	 * Deletes all data about comment
	 * 
	 * @param id
	 *            the identifier of comment
	 * @return true if comment was deleted
	 * @throws ServiceException
	 */
	public boolean deleteComment(Long id) throws ServiceException;

	/**
	 * Creates a new Comment
	 * 
	 * @param comment
	 *            comment to insert
	 * @return the identifier of inserted comment
	 * @throws ServiceException
	 */
	public Long createComment(Comment comment) throws ServiceException;

	/**
	 * Updates data about Comment
	 * 
	 * @param comment
	 *            the comment to update
	 * @return true if comment was updated
	 * @throws ServiceException
	 */
	public boolean updateComment(Comment comment) throws ServiceException;

	/**
	 * Receives the list of comments selected by the news identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return the list of comments
	 * @throws ServiceException
	 */
	public List<Comment> readCommentsByNewsId(Long newsId) throws ServiceException;

	/**
	 * Deletes data about Comments
	 * 
	 * @param commentIdList
	 *            the list of the comments identifiers
	 * @return true if all comments were deleted
	 * @throws ServiceException
	 */
	public Boolean deleteCommentsById(List<Long> commentIdList) throws ServiceException;

	/**
	 * Creates new Comments
	 * 
	 * @param commentList
	 *            list of comments which must be created
	 * @return the identifiers list of created comments
	 * @throws ServiceException
	 */
	public List<Long> createComments(List<Comment> commentList) throws ServiceException;
}
