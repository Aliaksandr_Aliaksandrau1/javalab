package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides service operations for tags
 * 
 */
public interface TagService {
	/**
	 * Returns tag by the identifier
	 * 
	 * @param id
	 *            the identifier of tag
	 * @return tag
	 * @throws ServiceException
	 */
	public Tag readTagById(Long id) throws ServiceException;

	/**
	 * Creates a new tag
	 * 
	 * @param tag
	 *            tag to insert
	 * @return the identifier of inserted tag
	 * @throws ServiceException
	 */
	public Long createTag(Tag tag) throws ServiceException;

	/**
	 * Updates data about tag
	 * 
	 * @param tag
	 *            the tag to update
	 * @return true if tag was updated
	 * @throws ServiceException
	 */
	public boolean updateTag(Tag tag) throws ServiceException;

	/**
	 * Deletes all data about tag
	 * 
	 * @param id
	 *            the identifier of tag
	 * @return true if tag was deleted
	 * @throws ServiceException
	 */
	public boolean deleteTag(Long id) throws ServiceException;

	/**
	 * Receives the list of tags selected by the news identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return the list of tags
	 * @throws ServiceException
	 */
	public List<Tag> readTagsByNewsId(Long newsId) throws ServiceException;

	/**
	 * Attaches the list of tags to the news by identifiers
	 * 
	 * @param newsId
	 *            the news identifier
	 * @param tagIdList
	 *            list of tag identifiers which should be attached to the news
	 * @return true if tags were attached, false otherwise
	 * @throws ServiceException
	 */
	public Boolean attachTagsToNews(Long newsId, List<Long> tagIdList) throws ServiceException;

}
