package com.epam.newsmanagement.exception;

public class DAOException extends NewsManagProjectException {

	private static final long serialVersionUID = -2689134211390170299L;

	public DAOException() {

	}

	public DAOException(String message) {
		super(message);
	}

	public DAOException(Throwable cause) {
		super(cause);
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}
}
