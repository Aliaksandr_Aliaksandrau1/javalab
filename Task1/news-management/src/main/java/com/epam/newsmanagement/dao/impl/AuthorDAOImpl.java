package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.utility.DaoUtility;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utility.ExceptionMess;
import com.epam.newsmanagement.utility.NamedConst;

public class AuthorDAOImpl implements AuthorDAO {

	private static final String AUTHOR_CREATE = "INSERT INTO authors (author_id, author_name, expired) VALUES (authors_seq.nextval, ?, ?)";
	private static final String AUTHOR_READ_BY_ID = "SELECT author_id, author_name, expired FROM authors WHERE author_id = ?";
	private static final String AUTHOR_UPDATE = "UPDATE authors SET author_name = ?, expired = ? WHERE author_id = ?";
	private static final String AUTHOR_DELETE = "DELETE FROM authors WHERE author_id = ?";
	private static final String AUTHOR_READ_BY_NEWS_ID = "SELECT a.author_id, a.author_name, a.expired FROM authors a JOIN news_authors na ON a.author_id=na.author_id WHERE news_id = ?";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Author readById(Long id) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Author author = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(AUTHOR_READ_BY_ID);
			pst.setLong(1, id);
			rs = pst.executeQuery();

			while (rs.next()) {
				author = new Author();
				author.setId(rs.getLong(NamedConst.AUTHOR_ID));
				author.setAuthorName(rs.getString(NamedConst.AUTHOR_NAME));
				author.setExpired(rs.getDate(NamedConst.AUTHOR_EXPIRED));
			}
		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.AUTHOR_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return author;
	}

	@Override
	public Long create(Author entity) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Long newAuthorId = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(AUTHOR_CREATE, new String[] { "author_id" });

			pst.setString(1, entity.getAuthorName());
			pst.setTimestamp(2, new Timestamp(entity.getExpired().getTime()));
			pst.executeUpdate();
			rs = pst.getGeneratedKeys();
			if (rs != null && rs.next()) {
				newAuthorId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.AUTHOR_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst);
		}
		return newAuthorId;
	}

	@Override
	public boolean delete(Long id) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean isDeleted = false;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(AUTHOR_DELETE);
			pst.setLong(1, id);
			if (pst.executeUpdate() == 1) {
				isDeleted = true;
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.AUTHOR_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return isDeleted;

	}

	@Override
	public boolean update(Author entity) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		boolean isUpdated = false;
		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(AUTHOR_UPDATE);
			pst.setString(1, entity.getAuthorName());
			pst.setTimestamp(2, new Timestamp(entity.getExpired().getTime()));
			pst.setLong(3, entity.getId());

			if (pst.executeUpdate() == 1) {
				isUpdated = true;
			}

		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.AUTHOR_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst);
		}

		return isUpdated;
	}

	@Override
	public Author readAuthorByNewsId(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Author author = null;

		try {
			con = dataSource.getConnection();
			pst = con.prepareStatement(AUTHOR_READ_BY_NEWS_ID);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();

			while (rs.next()) {
				author = new Author();
				author.setId(rs.getLong(NamedConst.AUTHOR_ID));
				author.setAuthorName(rs.getString(NamedConst.AUTHOR_NAME));
				author.setExpired(rs.getDate(NamedConst.AUTHOR_EXPIRED));
			}
		} catch (SQLException e) {
			throw new DAOException(ExceptionMess.AUTHOR_DAO_EXC + e, e);
		} finally {
			DaoUtility.closeCon(con, pst, rs);
		}

		return author;
	}

}
