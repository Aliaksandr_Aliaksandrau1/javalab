package com.epam.newsmanagement.service.impl;

import java.util.List;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utility.ExceptionMess;

public class TagServiceImpl implements TagService {

	private TagDAO tagDAO;

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	@Override
	public Tag readTagById(Long id) throws ServiceException {
		try {

			return tagDAO.readById(id);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Long createTag(Tag tag) throws ServiceException {
		try {
			return tagDAO.create(tag);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean updateTag(Tag tag) throws ServiceException {
		try {
			return tagDAO.update(tag);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean deleteTag(Long id) throws ServiceException {
		try {
			return tagDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public List<Tag> readTagsByNewsId(Long newsId) throws ServiceException {
		try {
			return tagDAO.readTagsByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Boolean attachTagsToNews(Long newsId, List<Long> tagIdList) throws ServiceException {
		try {
			return tagDAO.attachTagsToNews(newsId, tagIdList);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMess.TAG_SERVICE_EXC + e, e);
		}
	}

}
