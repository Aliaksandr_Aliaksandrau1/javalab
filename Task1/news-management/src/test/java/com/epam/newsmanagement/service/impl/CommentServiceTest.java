package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

	@Mock
	private CommentDAO commentDAO;
	private CommentServiceImpl commentService;
	private Comment comment = mock(Comment.class);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		commentService = new CommentServiceImpl();
		commentService.setCommentDAO(commentDAO);
		comment.setId(1L);
		comment.setNewsId(1L);
		comment.setCommentText("");
		// TODO добавить creationTime либо вообще убрать все set

	}

	@Test
	public void readCommentByIdTest() throws Exception {
		commentService.readCommentById(comment.getId());
		verify(commentDAO).readById(comment.getId());

	}

	@Test
	public void createCommentTest() throws Exception {
		commentService.createComment(comment);
		verify(commentDAO).create(comment);

	}

	@Test
	public void updateCommentTest() throws Exception {
		commentService.updateComment(comment);
		verify(commentDAO).update(comment);

	}
	
	@Test
	public void deleteCommentTest() throws Exception {
		commentService.deleteComment(comment.getId());
		verify(commentDAO).delete(comment.getId());

	}

}
