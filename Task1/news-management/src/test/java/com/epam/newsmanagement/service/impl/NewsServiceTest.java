package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

	@Mock
	private NewsDAO newsDAO;
	private NewsServiceImpl newsService;
	private News news = mock(News.class);
	private Author author = mock(Author.class);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		newsService = new NewsServiceImpl();
		newsService.setNewsDAO(newsDAO);
		news.setId(1L);
		author.setId(1L);
		// news.setAuthor(new Author());
		// news.setCommentList(new ArrayList<Comment>());
		// TODO add other data in setters

	}

	@Test
	public void readNewsByIdTest() throws Exception {
		newsService.readNewsById(news.getId());
		verify(newsDAO).readById(news.getId());

	}

	@Test
	public void createNewsTest() throws Exception {
		newsService.createNews(news);
		verify(newsDAO).create(news);

	}

	@Test
	public void updateNewsTest() throws Exception {
		newsService.updateNews(news);
		verify(newsDAO).update(news);

	}

	@Test
	public void deleteNewsTest() throws Exception {
		newsService.deleteNews(news.getId());
		verify(newsDAO).delete(news.getId());

	}

	@Test
	public void testSetNewsAuthor() throws Exception {
		newsService.setNewsAuthor(news.getId(), author.getId());
		verify(newsDAO).setNewsAuthor(news.getId(), author.getId());
	}
	@Test 
	public void testCountAllNews() throws Exception {
		newsService.countAllNews();
		verify(newsDAO).countAllNews();
	}

}
