package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.utility.DateUtility;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/dbunit_context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup(value = { "/CommentDAOTest_data.xml" }, type = DatabaseOperation.CLEAN_INSERT)
public class CommentDAOTest {
	
	@Autowired
	private CommentDAO commentDAO;

	@Test
	public void testReadById() throws Exception {
		Long commentId = new Long(1);
		Comment commentExp = commentBuilder(commentId, new Long(1), "comment text test 1", "2016-02-04 17:10:00");
		Comment commentAct = commentDAO.readById(commentId);
		assertComment(commentExp, commentAct);

	}

	@Test
	public void testDelete() throws Exception {

		Long id = new Long(2);
		commentDAO.delete(id);
		Comment commentAct = commentDAO.readById(id);
		Assert.assertNull(commentAct);

	}

	@Test
	public void testCreate() throws Exception {
		Long id = null;
		Comment commentExp = commentBuilder(id, new Long(1), "text create", "2012-06-17 00:00:00");
		id = commentDAO.create(commentExp);
		Comment commentAct = commentDAO.readById(id);
		commentExp.setId(id);
		assertComment(commentExp, commentAct);

	}

	@Test
	public void testUpdate() throws Exception {
		Long id = new Long(3);
		Comment commentExp = commentBuilder(id, new Long(2), "text update", "2014-06-17 00:00:00");
		commentDAO.update(commentExp);
		Comment commentAct = commentDAO.readById(id);
		assertComment(commentExp, commentAct);

	}

	@Test
	public void testReadCommentsByNewsId() throws Exception {
		Long newsId = new Long(1);
		List<Comment> commentListExp = new ArrayList<Comment>();
		Comment commentExp1 = commentBuilder(new Long(1), newsId, "comment text test 1", "2016-02-04 17:10:00");
		Comment commentExp2 = commentBuilder(new Long(2), newsId, "comment text test 2", "2015-02-10 17:10:00");
		commentListExp.add(commentExp1);
		commentListExp.add(commentExp2);
		List<Comment> commentListAct = commentDAO.readCommentsByNewsId(newsId);
		assertCommentList(commentListExp, commentListAct);

	}

	@Test
	public void testDeleteCommentsById() throws Exception {
		List<Long> commentIdList = new ArrayList<Long>();
		Long commId1 = new Long(1);
		Long commId2 = new Long(2);
		commentIdList.add(commId1);
		commentIdList.add(commId2);
		commentDAO.deleteCommentsById(commentIdList);

		Comment commentAct1 = commentDAO.readById(commId1);
		Comment commentAct2 = commentDAO.readById(commId2);
		Assert.assertNull(commentAct1);
		Assert.assertNull(commentAct2);

	}
	
	
	
	@Test
	public void testCreateComments() throws Exception {
		List<Long> commentIdList = null;
		Long comIdExp1 = null;
		Long comIdExp2 = null;
		List<Comment> commentList = new ArrayList<Comment>();
		
		Comment commentExp1 = commentBuilder(comIdExp1, new Long(1), "text create 1", "2012-06-17 00:00:00");
		Comment commentExp2 = commentBuilder(comIdExp2, new Long(1), "text create 2", "2012-06-18 00:00:00");
		
		commentList.add(commentExp1);
		commentList.add(commentExp2);
		
		commentIdList = commentDAO.createComments(commentList);
		
		
		// TODO rebuild test method
		
		//commentExp1.setId(commentIdList.get(0));
		//commentExp2.setId(commentIdList.get(1));
		
		
		
	}

	private void assertCommentList(List<Comment> commentListExp, List<Comment> commentListAct) {
		Assert.assertNotNull(commentListAct);
		Assert.assertEquals(commentListExp.size(), commentListAct.size());
		for (int i = 0; i < commentListExp.size(); i++) {
			assertComment(commentListExp.get(i), commentListAct.get(i));
		}
	}

	private void assertComment(Comment commentExp, Comment commentAct) {
		Assert.assertNotNull(commentAct);
		Assert.assertEquals(commentExp.getId(), commentAct.getId());
		Assert.assertEquals(commentExp.getNewsId(), commentAct.getNewsId());
		Assert.assertEquals(commentExp.getCommentText(), commentAct.getCommentText());
		// Assert.assertEquals(commentExp, commentAct); // TODO need in this
		// test?

	}

	private Comment commentBuilder(Long commentId, Long newsId, String commentText, String creationDate) {
		Comment comment = new Comment();
		comment.setId(commentId);
		comment.setNewsId(newsId);
		comment.setCommentText(commentText);
		comment.setCreationDate(DateUtility.dateFromString(creationDate));
		return comment;
	}

}
