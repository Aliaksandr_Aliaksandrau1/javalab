package com.epam.newsmanagement.dao.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.utility.DateUtility;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/dbunit_context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })

@DatabaseSetup(value = { "/NewsDAOTest_data.xml" }, type = DatabaseOperation.CLEAN_INSERT)
public class NewsDAOTest {

	@Autowired
	private NewsDAO newsDAO;

	@Test
	public void testReadById() throws Exception {
		Long id = new Long(1);
		News newsExp = newsBuilder(id, "title 1", "short 1", "full 1", "2015-06-15 00:00:00", "2015-06-17 00:00:00");
		News newsAct = newsDAO.readById(id);
		assertNews(newsExp, newsAct);

	}

	@Test
	public void testDelete() throws Exception {
		Long id = new Long(2);
		newsDAO.delete(id);
		News newsAct = newsDAO.readById(id);
		Assert.assertNull(newsAct);
	}

	@Test
	public void testUpdate() throws Exception {
		Long id = new Long(3);
		News newsExp = newsBuilder(id, "tit up", "st up", "ft up", "2012-06-15 00:00:00", "2012-06-17 00:00:00");
		newsDAO.update(newsExp);
		News newsAct = newsDAO.readById(id);
		assertNews(newsExp, newsAct);

	}

	@Test
	public void testCreate() throws Exception {
		Long id = null;
		News newsExp = newsBuilder(id, "tit cr", "st cr", "ft cr", "2012-06-15 00:00:00", "2012-06-17 00:00:00");
		id = newsDAO.create(newsExp);
		newsExp.setId(id);
		News newsAct = newsDAO.readById(id);
		assertNews(newsExp, newsAct);

	}

	@Test
	public void testSetNewsAuthor() throws Exception {
		Long newsId = new Long(4);
		Long authorId = new Long(4);
		boolean isSetted = newsDAO.setNewsAuthor(newsId, authorId);
		Assert.assertTrue(isSetted);
		// TODO remake test
	}
	
	@Test
	public void testCountAllNews () throws Exception {
		Long newsNumberExp = new Long(4);
		Long newsNumberAct = newsDAO.countAllNews();
		Assert.assertEquals(newsNumberExp, newsNumberAct);
		
	}
	
	private void assertNews(News newsExp, News newsAct) {
		Assert.assertNotNull(newsAct);
		Assert.assertEquals(newsExp.getId(), newsAct.getId());
		Assert.assertEquals(newsExp.getTitle(), newsAct.getTitle());
		Assert.assertEquals(newsExp.getShortText(), newsAct.getShortText());
		Assert.assertEquals(newsExp.getFullText(), newsAct.getFullText());
		Assert.assertEquals(newsExp.getCreationDate(), newsAct.getCreationDate());
		Assert.assertEquals(newsExp.getModificationDate(), newsAct.getModificationDate());
		Assert.assertEquals(newsExp, newsAct);
	}

	private News newsBuilder(Long newsId, String title, String shortText, String fullText, String crDate,
			String modDate) {
		News news = new News();
		news.setId(newsId);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(DateUtility.dateFromString(crDate));
		news.setModificationDate(DateUtility.dateFromString(modDate));
		return news;
	}

}
